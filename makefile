CC	:= gcc
LD	:= gcc

MODULES	:= shared_files local_server data_structures
SRC_DIR	:= $(addprefix src/,$(MODULES)) #gets file dirs
BUILD_DIR	:= $(addprefix build/,$(MODULES))

SRC	:= $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.c)) #concatenates dir with files contained inside it
OBJ       := $(patsubst src/%.c,build/%.o,$(SRC)) #get name of .o object
INCLUDES  := $(addprefix -I,$(SRC_DIR))

vpath %.c $(SRC_DIR) #adds src_dir to v_path so it can be found by make

define make-goal
$1/%.o: %.c
	$(CC) $(INCLUDES) -c $$< -o $$@
endef

.PHONY: all checkserverdirs clean

all: checkserverdirs build/server client auth

client:
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/errors.c -o build/shared_files/errors.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/request.c -o build/shared_files/request.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/response.c -o build/shared_files/response.o
	$(CC) -Isrc/data_structures -Isrc/client -c src/data_structures/list.c -o build/data_structures/list.o
	$(CC) -Isrc/data_structures -Isrc/shared_files -Isrc/client -c src/data_structures/HASH-table.c -o build/data_structures/HASH-table.o
	
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/client/KVS-lib.c -o build/client/KVS-lib.o
	$(CC) build/data_structures/list.o build/data_structures/HASH-table.o build/shared_files/errors.o build/shared_files/request.o build/shared_files/strings_manipulation.o build/client/KVS-lib.o build/shared_files/response.o -o build/KVS-lib.so -ldl -shared -fPIC

client3:
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/errors.c -o build/shared_files/errors.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/request.c -o build/shared_files/request.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/response.c -o build/shared_files/response.o
	$(CC) -Isrc/data_structures -Isrc/client -c src/data_structures/list.c -o build/data_structures/list.o
	$(CC) -Isrc/data_structures -Isrc/shared_files -Isrc/client -c src/data_structures/HASH-table.c -o build/data_structures/HASH-table.o
	
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/client/KVS-lib.c -o build/client/KVS-lib.o
	$(CC) -Isrc/shared_files -Isrc/client -c src/tests/two_clients.c -o build/client/two_clients.o
	$(CC) build/data_structures/list.o build/data_structures/HASH-table.o build/shared_files/errors.o build/shared_files/request.o build/shared_files/strings_manipulation.o build/client/KVS-lib.o build/client/two_clients.o build/shared_files/response.o -o build/two_clients

client2:
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/errors.c -o build/shared_files/errors.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/request.c -o build/shared_files/request.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/response.c -o build/shared_files/response.o
	$(CC) -Isrc/data_structures -Isrc/client -c src/data_structures/list.c -o build/data_structures/list.o
	$(CC) -Isrc/data_structures -Isrc/shared_files -Isrc/client -c src/data_structures/HASH-table.c -o build/data_structures/HASH-table.o
	
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/client/KVS-lib.c -o build/client/KVS-lib.o
	$(CC) -Isrc/shared_files -Isrc/client -c src/tests/8_clients_1000_inserts.c -o build/client/8_clients_1000_inserts.o
	$(CC) build/data_structures/list.o build/data_structures/HASH-table.o build/shared_files/errors.o build/shared_files/request.o build/shared_files/strings_manipulation.o build/client/KVS-lib.o build/client/8_clients_1000_inserts.o build/shared_files/response.o -o build/callback_program

client1:
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/errors.c -o build/shared_files/errors.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/request.c -o build/shared_files/request.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/response.c -o build/shared_files/response.o
	$(CC) -Isrc/data_structures -Isrc/client -c src/data_structures/list.c -o build/data_structures/list.o
	$(CC) -Isrc/data_structures -Isrc/shared_files -Isrc/client -c src/data_structures/HASH-table.c -o build/data_structures/HASH-table.o
	
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/client/KVS-lib.c -o build/client/KVS-lib.o
	$(CC) -Isrc/shared_files -Isrc/client -c src/tests/8_clients_10000_inserts.c -o build/client/8_clients_10000_inserts.o
	$(CC) build/data_structures/list.o build/data_structures/HASH-table.o build/shared_files/errors.o build/shared_files/request.o build/shared_files/strings_manipulation.o build/client/KVS-lib.o build/client/8_clients_10000_inserts.o build/shared_files/response.o -o build/operations_stress_test

clientui:
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/errors.c -o build/shared_files/errors.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/request.c -o build/shared_files/request.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/shared_files/response.c -o build/shared_files/response.o
	$(CC) -Isrc/data_structures -Isrc/client -c src/data_structures/list.c -o build/data_structures/list.o
	$(CC) -Isrc/data_structures -Isrc/shared_files -Isrc/client -c src/data_structures/HASH-table.c -o build/data_structures/HASH-table.o
	
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/client -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/client -c src/client/KVS-lib.c -o build/client/KVS-lib.o
	$(CC) -Isrc/shared_files -Isrc/client -c src/tests/client_UI.c -o build/client/client_UI.o
	$(CC) build/data_structures/list.o build/data_structures/HASH-table.o build/shared_files/errors.o build/shared_files/request.o build/shared_files/strings_manipulation.o build/client/KVS-lib.o build/client/client_UI.o build/shared_files/response.o -o build/client_UI

auth:
	$(CC) -Isrc/shared_files -Isrc/auth_server -c src/shared_files/errors.c -o build/shared_files/errors.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/auth_server -c src/shared_files/request.c -o build/shared_files/request.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/auth_server -c src/shared_files/response.c -o build/shared_files/response.o
	$(CC) -Isrc/data_structures -Isrc/auth_server -c src/data_structures/list.c -o build/data_structures/list.o
	$(CC) -Isrc/data_structures -Isrc/shared_files -Isrc/auth_server -c src/data_structures/HASH-table.c -o build/data_structures/HASH-table.o
	
	$(CC) -Isrc/shared_files -Isrc/auth_server -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/auth_server -c src/shared_files/strings_manipulation.c -o build/shared_files/strings_manipulation.o
	$(CC) -Isrc/shared_files -Isrc/data_structures -Isrc/auth_server -c src/auth_server/KVS-auth.c -o build/auth_server/KVS-auth.o
	$(CC) -Isrc/shared_files -Isrc/auth_server -Isrc/data_structures -c src/auth_server/main.c -o build/auth_server/main.o
	$(CC) build/data_structures/list.o build/data_structures/HASH-table.o build/shared_files/errors.o build/shared_files/request.o build/shared_files/strings_manipulation.o build/auth_server/KVS-auth.o build/auth_server/main.o build/shared_files/response.o -o build/auth_server_program

	

build/server: $(OBJ)
	$(LD) $^ -o $@

checkserverdirs: $(BUILD_DIR)

$(BUILD_DIR):
	@mkdir -p $@

clean:
	@rm -rf $(BUILD_DIR)

$(foreach bdir,$(BUILD_DIR),$(eval $(call make-goal,$(bdir))))
