#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include "errors.h"
#include "request.h"
#include "response.h"
#include "HASH-Table.h"
#include "strings_manipulation.h"

#define SOCKNAME "/tmp/local_server"
#define CALLBACK_SOCK "/tmp/callbacks"

typedef void (*callback_func)(char *);

int server_socket = -1;
int callback_sock = -1;
bool connected = false;
int internal_pipe[2];
hash_table * callbacks = NULL;
pid_t call_backpid = -1; // WASN'T SAVING THE PID OF THE CHILD PROCESS


/**
 * @brief Tries to connect to a socket, it retries, it waits exponentally more time for each attempt up to 2 minutes
 * 
 * @param domain socket domain
 * @param type socket type
 * @param protocol socket protocol
 * @param addr socket address
 * @param len socket len
 * @return int on a success returns a file descriptor of a socket on a failed throws errors for failed socket creation or connection failed  
 */
int connect_retry(int domain, int type, int protocol, struct sockaddr_un *addr, socklen_t len) {
    int numsec, fd;
    //connect with exponential backoff
    for (numsec = 1; numsec <= MAXSLEEP; numsec <<= 1) {
        if ((fd = socket(domain, type, protocol)) < 0)
            return SOCK_CREATION_FAILED;
        if (connect(fd, (const struct sockaddr *) addr, len) == 0) {
            
            return fd;
        }
        close(fd);
        //Delay before trying again.
        if (numsec <= MAXSLEEP / 2)
            sleep(numsec);
    }
    return CONNECTION_FAILED;
}

/**
 * @brief Tries to establish a connection to the server, uses the connect_retry
 * 
 * @param group_id is a string with the group name
 * @param secret is a string with the group password
 * @return 0 if it is a success, on fail throws errors for failed socket creation, failed connection, thread creation failed, group not found, wrong secret 
 */
int establish_connection(char *group_id, char *secret) {
    if (connected == true) {
        return ALREADY_CONNECTED;
    }
    int error;
    printf("Trying to connect to server....\n");
    signal(SIGPIPE, SIG_IGN);
    callback_sock = -1;
    struct sockaddr_un server_addr;
    hash_table * aux;
    if(create_table(&aux) < 0)
        return FAILED_CREATION;
    callbacks = aux;
    //clean mem
    memset(&server_addr, 0, sizeof(struct sockaddr_un));
    //socket initialization
    server_addr.sun_family = AF_UNIX;
    sprintf(server_addr.sun_path, SOCKNAME);
    //try to connect to sever

    if ((server_socket = connect_retry(AF_UNIX, SOCK_STREAM, 0, &server_addr, sizeof(server_addr))) < 0) {
        //couldn't connect sock variable has the error code
        return server_socket;
    }
    //send request to server
    request_ls req = create_request_ls(CONNECTION_REQUEST, 2);
    error = send_request_ls(req, group_id, secret, server_socket);
    if (error < 0){
        connected = false;
        return error;
    }
    char * res = NULL;
    if ((error = read_response_ls(server_socket, &res)) < 0){
        if (error == READ_ERROR) {
            connected= false;
        }
        return error;
    }
    connected = true;
    printf("Connected\n");
    if (res != NULL) { //VERIFICATION WAS MISSING
        free(res);
    }
    return 0;
}
/**
 * @brief Sends a request to a server to keep track of a key value pair 
 * 
 * @param key key used to access the key value pair
 * @param value value to be saved that can only be access with the key
 * @return int returns 1 on a success on fail returns an error associated with either a failed attempt
 *         to read or send to and from the client or an error sent from the client regarding the request
 */
int put_value(char *key, char *value) {
    if (connected == false) {
        return NOT_CONNECTED;
    }
    int error;
    request_ls req = create_request_ls(PUT_VALUE, 2);
    char * res = NULL;
    error = send_request_ls(req, key, value, server_socket);
    if (error < 0) {
        connected = false;
        return error;
    }
    
    if((error = read_response_ls(server_socket, &res)) < 0 ){
        if (error == READ_ERROR) {
            connected= false;
        }
        return error;
    }
    if (res != NULL) { //VERIFICATION WAS MISSING
        free(res);
    }
    return 1;
}

/**
 * @brief Get the value associated with a key
 * 
 * @param key key used to access the key value pair
 * @param value returns the string found if it exits
 * @return int returns 1 on a success on fail returns an error associated with either a failed attempt
 *         to read or send to and from the client or an error sent from the client regarding the request
 */
int get_value(char *key, char **value) {
    if (connected == false) {
        return NOT_CONNECTED;
    }
    int error;
    * value = NULL;
    request_ls req = create_request_ls(GET_VALUE, 1);
    error = send_request_ls(req, key, NULL, server_socket);
    if (error < 0){
        connected = false;
        return error;
    }
    
    if((error = read_response_ls(server_socket, value)) < 0 ){
        if (error == READ_ERROR) {
            connected= false;
        }
        return error;
    }
    return 1;
}

/**
 * @brief deletes a key value pair
 * 
 * @param key key used to access the key value pair
 * @return int returns 1 on a success on fail returns an error associated with either a failed attempt
 *         to read or send to and from the client or an error sent from the client regarding the request
 */
int delete_key(char *key) {
    if (connected == false) {
        return NOT_CONNECTED;
    }
    int error;
    request_ls req = create_request_ls(DELETE_VALUE, 1);
    char * res = NULL;
    error = send_request_ls(req, key, NULL, server_socket);
    if (error < 0) {
        connected = false;
        return error;
    }
    if((error = read_response_ls(server_socket, &res)) < 0 ){
        if (error == READ_ERROR) {
            connected= false;
        }
        return error;
    }
    if (res != NULL) { //VERIFICATION WAS MISSING
        free(res);
    }
    return 1;
}

void free_callback(void * callback){
    return;
}

void callback_handler(int signum){
    if (signum != SIGUSR1) {
        printf("ERROR\n");
        return;
    }
    char key[128];
    callback_func func;
    void * val;
    if(read(internal_pipe[0], key, 25) > 0) {
        if (ht_search(callbacks, key, &val) == 0) {
            func = (callback_func) val;
            func(key);
        }
    }
    return;
}

//REMOVED TWO PRINTS TWO AVOID SPAM
void read_process(void){
    char key[128];
    pid_t ppid = getppid();
    while (read(callback_sock, key, 25) > 0) {
        write(internal_pipe[1], key, 25);
        kill(ppid, SIGUSR1);
    }
    return;
}

/**
 * @brief registers a function to a key if a key is changed the callback function is called
 * 
 * @param key key used to access the key value pair
 * @param callback_function function to be called if the value is changed
 * @return int returns 1 on a success on fail returns an error associated with either a failed attempt
 *         to read or send to and from the client or an error sent from the client regarding the request 
 */
int register_callback(char *key, void (*callback_function)(char *)) {
    if (connected == false) {
        return NOT_CONNECTED;
    }
    if (callbacks == NULL) {
        return HASHTABLE_NOT_FOUND;
    }
    int error;
    char * tmp_key;
    error = alloc_string(&tmp_key, (const char *) key);
    if (error < 0) {
        return FAILED_MALLOC;
    }
    request_ls req = create_request_ls(REGISTER_CALLBACK, 1);
    char * res = NULL;
    error = send_request_ls(req, tmp_key, NULL, server_socket);
    if (error < 0) {
        connected = false;
        return error;
    }
    if((error = read_response_ls(server_socket, &res)) < 0 ){
        if (error == READ_ERROR) {
            connected= false;
        }
        return error;
    }
    
    if (res == NULL) {
        return FAILED_REGISTER_CALLBACK;
    }
    //try to connect to sever
    
    if (callback_sock < 0) {
        struct sockaddr_un server_addr;
        //clean mem
        memset(&server_addr, 0, sizeof(struct sockaddr_un));
        //socket initialization
        server_addr.sun_family = AF_UNIX;
        sprintf(server_addr.sun_path, CALLBACK_SOCK);
        if ((callback_sock = connect_retry(AF_UNIX, SOCK_STREAM, 0, &server_addr, sizeof(server_addr))) < 0) {
            //couldn't connect sock variable has the error code
            return callback_sock;
        }
    
        if(pipe(internal_pipe) == -1){
            return UNKNOWN;
        }
        signal(SIGUSR1, callback_handler);
        if((call_backpid = fork()) == 0){ //WASN SAVING CALLBACK PID NEEDED TO TERMINATE CHILD PROCESS
            read_process();
        }
        
    }
    
    if((error = ht_insert(&callbacks, tmp_key, callback_function, free_callback)) < 0)
        return error;
    if (error == RESIZE_HT) {
        if ((error = ht_resize(&callbacks, free_callback)) < 0) {
            return error;
        }
    }

    free(res);
    if (error < 0) {
        return error;
    }
    
    return 1;
}

/**
 * @brief closes the connection with the server
 * 
 * @return int returns 1 on a success on fail returns an error associated with either a failed attempt
 *         to read or send to and from the client or an error sent from the client regarding the request 
 */
int close_connection(void) {
    if (connected == false) {
        return NOT_CONNECTED;
    }
    int error;
    char * res = NULL;
    request_ls req = create_request_ls(CLOSE_REQUEST, 0);
    error = send_request_ls(req, NULL, NULL, server_socket);
    if (error < 0) {
        connected = false;
    }
    if((error = read_response_ls(server_socket, &res)) < 0 )
        if (error == READ_ERROR) {
            connected = false;
        }
    close(server_socket);
    close(callback_sock);
    if (call_backpid > 0) { //WASN'T KILLING THE CHILD PROCESS
        kill(call_backpid, SIGKILL);
    }
    call_backpid = -1;
    hash_table * aux = (hash_table *)callbacks;
    free_table(aux, free_callback);
    free(res);
    connected = false; //WAS MISSING
    printf("Closing Connection\n"); //ADDED FOR READABILITY
    return 1;
}



