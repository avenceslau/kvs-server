#ifndef HASH_Table_h
#define HASH_Table_h

#define HT_INITIAL_SIZE 5381
#define RESIZE_HT 8000
#define KEY_SZ_LIMIT 128

typedef struct hash_item hash_item;
typedef struct hash_table hash_table;

/**
 * @brief structure that holds an hash item
 * 
 */
struct hash_item {
    char * key;
    void * value;
    struct hash_item * next;
};

/**
 * @brief definition of hash table object
 *        items array of hash items structure
 *        size max numbers of eelements that can be inserted
 *        count number of items currently inserted
 * 
 */
struct hash_table {
    hash_item** items;
    unsigned long size;
    unsigned long count;
};

/**
 * @brief inserts a key value pair in the hash table
 * 
 * @param table pointer to hash table
 * @param key key to accesss value
 * @param item item to be insert
 * @return int 
 */
int ht_insert(hash_table** table, char* key, void* item, void (*free_item)(void * item));

/**
 * @brief searches for a value on an hash table associated to a key
 * 
 * @param table pointer to a hash table
 * @param key key used to access a value
 * @param value value that will be fetched and returned
 * @return int returns 0 on a success or an error if there is no value or the key doesn't exist and the value is returned if found
 */
int ht_search(hash_table* table, char* key, void ** value);

/**
 * @brief Create a hash table object
 * 
 * @param table returns the hash table
 * @return int 0 if successful or failed malloc if there is not enough space
 */
int create_table(hash_table ** table);

/**
 * @brief searches for and deletes a hash item associated with a key, deletes the key and returns the value or an error if any occur
 * 
 * @param table pointer to the hash_table
 * @param key key of the item that will be deleted
 * @param value return value
 * @return int an error forno hash table no key, item not found or a 0 on a success
 */
int delete_key_return_value(hash_table * table, char * key, void ** value);

/**
 * @brief resizes the hash table to accomodate more items
 * 
 * @param table pointer to hash table that will be resized
 * @return int returns a 0 on success or an error on a failed
 */
int ht_resize(hash_table ** table, void (*free_item)(void * item));

/**
 * @brief frees an hash item and returns the value of said item
 * 
 * @param item the hash item that will be freed
 * @return void* 
 */
void * free_hash_item(hash_item * item);

/**
 * @brief frees a hash table and all items inside it
 * 
 * @param table pointer to hash table object
 * @param free_item tules to free the items inside the hash_table
 */
void free_table(hash_table* table, void (*free_item)(void * item));

/**
 * @brief returns the number of items in an hash table
 * 
 * @param table pointer to the table
 * @return int returns the nyber of items on an hash table
 */
unsigned long number_of_items(hash_table * table);

#endif /* HASH_Table_h */
