#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "errors.h"
#include "HASH-Table.h"


int find_next_size(unsigned long previous_size, unsigned long add_more, unsigned long * new_size);
unsigned long hash_function(char* key, unsigned long ht_size);
int create_hash_item(hash_item ** item, char * key, void * value);
int create_table(hash_table ** table);
void free_table(hash_table* table, void (*free_item)(void * item));
void * free_hash_item(hash_item * item);
int delete_key(hash_table * table, char * key);
int delete_key_return_value(hash_table * table, char * key, void ** value);
void handle_collision(hash_table* table, unsigned long index, hash_item* item);
int ht_resize(hash_table ** table, void (*free_item)(void * item));
int ht_search(hash_table* table, char* key, void ** value);
void print_search(hash_table* table, char* key);
unsigned long number_of_items(hash_table * table);
int ht_insert(hash_table** table, char* key, void* item, void (*free_item)(void * item));

/**
 * @brief finds the next hash table size by finding the next prime number that is at least more than
 *        add_more bigger
 * 
 * @param previous_size hash table previous size
 * @param add_more how many more entries are required atleast
 * @param new_size return value for the new size
 * @return int 0 if able to resize or an error if there is no space to alloc the size needed to find the primes, new size returns the new size
 */
int find_next_size(unsigned long previous_size, unsigned long add_more, unsigned long * new_size){
    unsigned long next_size = previous_size + add_more;
    unsigned long * primes = (unsigned long *) malloc(sizeof(unsigned long)*next_size);
    if (primes == NULL) {
        return FAILED_MALLOC;
    }
    for (unsigned long i = 0 ; i < next_size ; i++) {
        primes[i] = 1;
    }
    unsigned long limit = sqrt(next_size) + 1;
    for (int i=2; i<limit; i++) {
        if (primes[i-1]) {
            for (int j=i*i; j<=next_size; j+=i) {
                primes[j-1] = 0;
            }
        }
    }
    
    unsigned long tmp_next_size = 0;
    for (unsigned long i=next_size; i >= 2 ; i--) {
        if (primes[i-1]) {
            tmp_next_size = i;
            break;
        }
    }
    free(primes);
    if (tmp_next_size == previous_size) {
        return find_next_size(previous_size, previous_size + add_more, new_size);
    }
    *new_size = tmp_next_size;
    return 0;
}

/**
 * @brief calculates the index for a key
 * 
 * @param key   key to be hashed
 * @param ht_size size of the hash table
 * @return unsigned long index associated with a key
 */
unsigned long hash_function(char* key, unsigned long ht_size) {
    unsigned long hash = ht_size;
    int c;

    while ((c = *key++))
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

    return hash % ht_size;
}

/**
 * @brief Create a hash item object, key and value need to be previously allocated, item returns the item
 * 
 * @param item variable through where the item is returned
 * @param key key for the hash item
 * @param value value associated with the key
 * @return int 0 if successful or an error for failed allocation, key with no value or empty value
 */
int create_hash_item(hash_item ** item, char * key, void * value){
    if (key == NULL) {
        return KEY_HAS_NOVALUE;
    }
    if (value == NULL) {
        return VALUE_ERROR;
    }
    
    (*item) = (hash_item *) malloc(sizeof(hash_item));
    
    if (*item == NULL) {
        return FAILED_MALLOC;
    }
    (*item)->key = key;
    (*item)->value = value;
    (*item)->next = NULL;
    return 0;
}

/**
 * @brief Create a hash table object
 * 
 * @param table returns the hash table
 * @return int 0 if successful or failed malloc if there is not enough space
 */
int create_table(hash_table ** table) {
    // Creates a new HashTable
    (*table) = (hash_table*)malloc(sizeof(hash_table));

    if (table == NULL)
        return FAILED_MALLOC;
    
    (*table)->size = HT_INITIAL_SIZE;
    (*table)->count = 0;
    (*table)->items = (hash_item**) calloc((*table)->size, sizeof(hash_item*));
    if ((*table)->items == NULL) {
        free(table);
        return FAILED_MALLOC;
    }
    
    for (int i = 0; i < (*table)->size; i++)
        (*table)->items[i] = NULL;

    return 0;
}

/**
 * @brief frees a hash table and all items inside it
 * 
 * @param table pointer to hash table object
 * @param free_item tules to free the items inside the hash_table
 */
void free_table(hash_table* table, void (*free_item)(void * item)) {
    void * aux;
    for (int i = 0; i < table->size; i++) {
        hash_item* item = table->items[i], * tmp;
        while (item != NULL) {
            tmp = item->next;
            aux = free_hash_item(item);
            free_item(aux);
            item = tmp;
        }
    }
    free(table->items);
    free(table);
}


/**
 * @brief frees an hash item and returns the value of said item
 * 
 * @param item the hash item that will be freed
 * @return void* 
 */
void * free_hash_item(hash_item * item) {
    void * aux = item->value;
    free(item->key);
    free(item);
    return aux;
}

/**
 * @brief searches for and deletes a hash item associated with a key, deletes the key and returns the value or an error if any occur
 * 
 * @param table pointer to the hash_table
 * @param key key of the item that will be deleted
 * @param value return value
 * @return int an error forno hash table no key, item not found or a 0 on a success
 */
int delete_key_return_value(hash_table * table, char * key, void ** value){
    if (table == NULL) {
        return HASHTABLE_NOT_FOUND;
    }
    if(key == NULL)
        return KEY_HAS_NOVALUE;
    unsigned long index = hash_function(key, table->size);
    hash_item* item = table->items[index];
    if (item == NULL)
        return KEY_NOT_FOUND;
    
    
    hash_item * aux = item;
    while (item != NULL) {
        if (strcmp(key, item->key) == 0) {
            if (aux == item) {
                table->items[index] = item->next;
                *value = free_hash_item(item);
                return 1;
            }
            aux->next = item->next;
            *value = free_hash_item(item);
            return 1;
        }
        aux = item;
        item = item->next;
    }
    return KEY_NOT_FOUND;
}



/**
 * @brief resolves a collision, on a collision the item is inserted on a list associated with the index
 * 
 * @param table pointer to hash table
 * @param index index of where the collision happened
 * @param item the item that is being inserted
 */
void handle_collision(hash_table* table, unsigned long index, hash_item* item) {
    hash_item * runner = table->items[index];
    while (item->next != NULL) {
        //if key already exists update value
        if (strcmp(runner->key, item->key) == 0) {
            free(runner->value);
            runner->value = item->value;
            free(item->key);
            free(item);
            return;
        }
    }
    item->next = table->items[index];
    table->items[index] = item;
    return;
}

/**
 * @brief Frees only the pointers to hash items of the hashtable
 *
 * @param table hashtable to be freed
 */
void free_table_resize(hash_table* table) {
    for (int i = 0; i < table->size; i++) {
        hash_item* item = table->items[i], * tmp;
        while (item != NULL) {
            tmp = item->next;
            free(item);
            item = tmp;
        }
    }
    free(table->items);
    free(table);
}


/**
 * @brief resizes the hash table to accomodate more items
 * 
 * @param table pointer to hash table that will be resized
 * @return int returns a 0 on success or an error on a failed
 */
int ht_resize(hash_table ** table, void (*free_item)(void * item)) {
    // Creates a new HashTable
    hash_table* new_table = (hash_table*)malloc(sizeof(hash_table));
    if (new_table == NULL)
        return FAILED_MALLOC;
    if (find_next_size((*table)->size, HT_INITIAL_SIZE, &new_table->size)) {
        free(new_table);
        return FAILED_RESIZE;
    }
    
    new_table->count = 0;
    new_table->items = (hash_item**) calloc(new_table->size, sizeof(hash_item*));
    if (new_table->items == NULL) {
        free(new_table);
        return FAILED_MALLOC;
    }
    
    for (int i = 0; i < new_table->size; i++)
        new_table->items[i] = NULL;

    hash_item * item;
    
    for (int i = 0; i < (*table)->size; i++) {
        item = (*table)->items[i];
        while (item != NULL){
            if(ht_insert(&new_table, item->key, item->value, free_item) < 0){
                free_table_resize(new_table);
                return UNKNOWN;
            }
            item = item->next;
        }

    }
    
    free_table_resize((*table));
    (*table) = new_table;
    return 0;
}

/**
 * @brief inserts a key value pair in the hash table
 * 
 * @param table pointer to hash table
 * @param key key to accesss value
 * @param item item to be insert
 * @return int 
 */
int ht_insert(hash_table** table, char* key, void* item, void (*free_item)(void * item)) {
    if (*table == NULL) {
        return HASHTABLE_NOT_FOUND; 
    }
    
    if (key == NULL)
        return KEY_HAS_NOVALUE;
    
    if ((*table)->size == (*table)->count) {
        return HASHTABLE_FULL;
    }
    
    unsigned long index = hash_function(key, (*table)->size);  //Calculate index
    
    hash_item* current_item = (*table)->items[index];
    if (current_item != NULL) {
        if (strcmp(current_item->key, key) == 0) {
            free_item((*table)->items[index]->value);
            (*table)->items[index]->value = item;
            return 0;
        }
        current_item = current_item->next;
    }
    current_item = (*table)->items[index];
    hash_item * aux;
    if (create_hash_item(&aux, key, item) < 0) {
        free(key);
        free_item(item);
        return FAILED_ITEM_CREATION;
    }
    
    if (current_item == NULL) {
        (*table)->items[index] = aux;
    } else {
            handle_collision(*table, index, aux);
    }
    (*table)->count++;
    if ((*table)->count == (*table)->size){
        return RESIZE_HT;
    }
    return 0;
}

/**
 * @brief searches for a value on an hash table associated to a key
 * 
 * @param table pointer to a hash table
 * @param key key used to access a value
 * @param value value that will be fetched and returned
 * @return int returns 0 on a success or an error if there is no value or the key doesn't exist and the value is returned if found
 */
int ht_search(hash_table* table, char* key, void ** value) {
    if (table == NULL) {
        return HASHTABLE_NOT_FOUND;
    }
    if (key == NULL) {
        return KEY_NOT_FOUND;
    }
    unsigned long index = hash_function(key, table->size);
    hash_item* item = table->items[index];
    
    if (item == NULL){
        *value = NULL;
        return KEY_HAS_NOVALUE;
    }
    while (item != NULL) {
        if (strcmp(key, item->key) == 0) {
            *value = item->value;
            return 0;
        }
        item = item->next;
    }
    return KEY_HAS_NOVALUE;
}

/**
 * @brief prints a value associated with a key
 * 
 * @param table pointer to the hash table
 * @param key key of the value that will be print
 */
void print_search(hash_table* table, char* key) {
    void * val;
    
    if (ht_search(table, key, &val) < 0) {
        printf("%s does not exist\n", key);
        return;
    } else {
        printf("Key:%s, Value:%s\n", key, (char *) val);
    }
}

/**
 * @brief returns the number of items in an hash table
 * 
 * @param table pointer to the table
 * @return int returns the nyber of items on an hash table
 */
unsigned long number_of_items(hash_table * table){
    if(table == NULL){
        return HASHTABLE_NOT_FOUND;
    }
    return table->count;
}
