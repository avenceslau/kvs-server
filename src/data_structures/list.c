//
//  list.c
//  server.c
//
//  Created by André Venceslau on 26/05/2021.
//

#include "list.h"
#include <stdlib.h>

typedef struct list list;
typedef struct list_item list_item;

struct list_item{
    void * item;
    list_item * next;
    list_item * previous;
};

struct list{
    void *(*create_item)(void *);
    void (*free_item)(void *);
    int (*get_key_item)(void *);
    void (*free_list)(list *);
    void *(*print_item)(void *);
    void (*modify_item) (void *, void *); // pointer to the item that will be modified, pointer to a new item that has the modifications
    list_item * head;
    list_item * tail;
};

/**
 * @brief creates a new list item and also creates the item that will be inserted
 * 
 * @param item item that will be added
 * @param l pointer to a list 
 * @return list_item* null on an error or the pointer to the list item
 */
list_item * new_item(void * item, list * l){
    list_item * new_item = (list_item *) malloc(sizeof(list_item));
    if (new_item == NULL) {
        return NULL;
    }
    new_item->item = l->create_item(item);
    if (new_item->item == NULL) {
        free(new_item);
        return NULL;
    }
    new_item->next = NULL;
    new_item->previous = l->tail;
    return new_item;
}

/**
 * @brief adds an item to the list, handles all the aloocations necessary
 * 
 * @param l pointer to list
 * @param item pointer to the item that will be added shlould be passed as reference
 */
void add_to_list(list * l, void * item){
    if(item == NULL)
        return;
    if (l == NULL) {
        return;
    }
    if (l->head == NULL) {
        l->head = new_item(item, l);
        l->tail = l->head;
        return;
    }
    l->tail->next = new_item(item, l);
    l->tail = l->tail->next;
}

/**
 * @brief frees a list
 * 
 * @param l pointer to the list
 */
void free_list(list * l){
    list_item * runner = l->head;
    while (runner != NULL) {
        l->free_item(runner->item);
        //THE FREE WAS WRONG
        runner->item = NULL;
        l->head = runner->next;
        if (runner != NULL) {
            free(runner);
        }
        runner = l->head;
    }
    if (l != NULL) {
        free(l);
    }
    l->head = NULL;
    l->tail = NULL;
    l = NULL;
}

/**
 * @brief Create a list object
 * 
 * @param create_item function that describes how an item is created, the item shoul be allocated inside and shoulbe passed as reference
 * @param free_item rules on how to free the item
 * @param get_key finds the indentifier of an item
 * @param print_item prints the item receibes a void * that countains the item that will be printed
 * @param modify_item rules on how to modify an item
 * @return list* returns null on a failure and returs the pointer to the list on a success 
 */
list * create_list(void *(*create_item)(void *), void free_item(void *), int get_key(void *), void *print_item(void *), void modify_item(void *, void *)){
    list * l = (list *) malloc(sizeof(list));
    if (l == NULL)
        return NULL;
    
    l->create_item = create_item;
    l->free_item = free_item;
    l->free_list = free_list;
    l->get_key_item = get_key;
    l->modify_item = modify_item;
    l->print_item = print_item;
    l->head = NULL;
    l->tail = NULL;
    return l;
}

/**
 * @brief searches for a list item
 * 
 * @param key identifier of the item
 * @param l pointer to the list
 * @return list_item* returns the list item if found or null on a failure
 */
list_item * find_list_item(int key, list * l){
    list_item * runner;
    runner = l->head;
    while (runner != NULL) {
        if (key == l->get_key_item(runner->item)) {
            return runner;
        }
        runner = runner->next;
    }
    return NULL;
}

/**
 * @brief finds the item stored on a list
 * 
 * @param key indentifier of the item
 * @param l pointer to the list
 * @return void* returns the item or null
 */
void * find_item(int key, list * l){
    list_item * aux = find_list_item(key, l);
    if (aux == NULL)
        return NULL;
    
    return aux->item;
}

/**
 * @brief removes an item from the list
 * 
 * @param key identifier of the item that wille deleted
 * @param l pointer to the lsit
 * @return void* returns the item that was on the list and removes it, or returns null if not found
 */
void * remove_item_from_list(int key, list * l){
    list_item * item = find_list_item(key, l);
    list_item * aux = NULL;
    void * res;
    if (item == NULL)
        return NULL;
    if (item == l->tail && item == l->head) {
        l->head = NULL;
        l->tail = NULL;
        res = item->item;
        free(item);
        return res;
    }
    if (item == l->head) {
        if (item->next != NULL) {
            aux = item->next;
            aux->previous = NULL;
        }
        l->head = aux;
        res = item->item;
        free(item);
        return res;
    }
    if (item == l->tail) {
        if (item->previous != NULL) {
            item->previous->next = NULL;
            aux = item->previous;
        }
        l->tail = aux;
        res = item->item;
        free(item);
        return res;
    }
    item->previous->next = item->next;
    item->next->previous = item->previous;
    res = item->item;
    free(item);
    return res;
}

/**
 * @brief removes an item from the list
 *
 * @param l pointer to the lsit
 * @return void* returns the item that was on the list and removes it, or returns null if not found
 */

list_item * remove_item_from_list_receives_item(list_item * item, list * l){
    list_item * aux = NULL;
    void * res;
    if (item == NULL)
        return NULL;
    if (item == l->tail && item == l->head) {
        l->head = NULL;
        l->tail = NULL;
        res = item->item;
        free(item);
        l->free_item(res);
        return NULL;
    }
    if (item == l->head) {
        if (item->next != NULL) {
            aux = item->next;
            aux->previous = NULL;
        }
        l->head = aux;
        res = item->item;
        l->free_item(res);
        return aux;
    }
    if (item == l->tail) {
        if (item->previous != NULL) {
            aux = item->previous;
            aux->next = NULL;
        }
        l->tail = aux;
        res = item->item;
        l->free_item(res);
        return NULL;
    }
    aux = item->next;
    item->previous->next = item->next;
    item->next->previous = item->previous;
    res = item->item;
    l->free_item(res);
    return aux;
}

/**
 * @brief prints the whole list
 * 
 * @param l pointer to the list that will be printed
 */
void print_list(list * l){
    if(l == NULL)
        return;
    list_item * runner = NULL;
    void * aux;
    runner = l->head;
    while (runner != NULL) {
        aux = l->print_item(runner->item);
        if (aux == NULL) {
            runner = remove_item_from_list_receives_item(runner, l);
        } else {
        runner = runner->next;
        }
    }
}

/**
 * @brief mofifies an item
 * 
 * @param key item identifier
 * @param modified_item item with the corresponding modificatons
 * @param l pointer to the list
 */
void modify_item(int key, void * modified_item, list * l){
    void * item = find_item(key, l);
    if (item == NULL) {
        return;
    }
    l->modify_item(item, modified_item);
}
