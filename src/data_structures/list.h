//
//  list.h
//  server.c
//
//  Created by André Venceslau on 26/05/2021.
//

#ifndef list_h
#define list_h

#include <stdio.h>
#include <stdbool.h>

typedef struct list list;
void * find_item(int key, list * l);
void * remove_item_from_list(int key, list * l);
list * create_list(void *(*create_item)(void *), void free_item(void *), int get_key(void *), void *print_item(void *), void modify_item(void *, void *));
void free_list(list * l);
void add_to_list(list * l, void * item);
void print_list(list * l);

#endif /* list_h */
