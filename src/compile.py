import os
from subprocess import call

def listToString(s): 
    # initialize an empty string
    str1 = " " 
    
    return (str1.join(s))

def getFilenamesInDir(directory):
    filenames = [directory +"/"+ t for t in os.listdir(directory) if t.endswith('.c')]
    return filenames

if __name__ == "__main__":

    main_dir = os.getcwd()
    
    hash_t = listToString(getFilenamesInDir(main_dir))

    shared_dir = main_dir + "/shared_files"
    shared_files = listToString(getFilenamesInDir(shared_dir))
    
    client_dir = main_dir + "/client"
    client_files = listToString(getFilenamesInDir(client_dir))
    
    server_dir = main_dir + "/local_server"
    server_files = listToString(getFilenamesInDir(server_dir))
    
    auth_dir = main_dir + "/auth_server"
    auth_files = listToString(getFilenamesInDir(auth_dir))
    
    program_name = ["client","server","auth"]
    
    flags = "-Wall -lpthread -o"

    client = f'gcc {client_files} {shared_files} {flags} {program_name[0]}'
    server = f'gcc {server_files} {shared_files} {hash_t} {flags} {program_name[1]}'
    auth = f'gcc {auth_files} {shared_files} {hash_t} {flags} {program_name[2]}'

    call(client,shell=True)
    call(server,shell=True)
    #call(cmd1,shell=True)