//
//  main.c
//  client
//
//  Created by André Venceslau on 12/05/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "errors.h"
#include "KVS-lib.h"

void f1(char * str){
    printf("\n\t\tCallback: %s\n",str);
}

void print_user_options(void) {
    printf("--------------//---------------\n");
    printf("Commands:\n");
    printf("1 - Connect to group (up to 50 characters)\n");
    printf("2 - Insert KV pair\n");
    printf("3 - Register callback\n");
    printf("4 - Delete KV pair\n");
    printf("5 - Get KV pair\n");
    printf("6 - Close Connection\n");
    printf("9 - Quit\n");
    printf("--------------//---------------\n");
    return;
}

void read_group_id(char group_id[50]) {
    printf("Insert a group name: ");
    scanf("%s", group_id);
    printf("\n");
}

void read_message(char * str, const char * msg) {
    printf("%s", msg);
    scanf("%s", str);
    printf("\n");
}

int check_command(char * command){
    if(strcmp(command,"1") == 0)
        return 1;
     if(strcmp(command,"2") == 0)
        return 2;
    if(strcmp(command,"3") == 0)
        return 3;
    if(strcmp(command,"4") == 0)
        return 4;
    if(strcmp(command,"5")  == 0)
        return 5;
    if(strcmp(command,"6")  == 0)
        return 6;
    if(strcmp(command,"9")  == 0)
        return 9;
        
    return 11;
}

void UI(void) {
    char command[5];
    char group_id[50];
    char secret[25];
    char key[128];
    char value[8196];
    char * value_catcher;
    printf("Welcome to test mode\n\n");
    while (1) {
        print_user_options();
        printf("\nOption: ");
        scanf("%s", command);
        printf("\n");
        switch(check_command(command)){
            case 1:
                read_message(group_id, "Insert group name: ");
                read_message(secret, "Insert the secret: ");
                catch_error(establish_connection(group_id, secret));
                break;
            case 2:
                read_message(key, "Insert a key: ");
                read_message(value, "Insert a value: ");
                catch_error(put_value(key, value));
                break;
            case 3:
                read_message(key, "Insert a key: ");
                catch_error(register_callback(key, f1));
                break;
            case 4:
                read_message(key, "Insert a key: ");
                catch_error(delete_key(key));
                break;
            case 5:
                read_message(key, "Insert a key: ");
                catch_error(get_value(key, &value_catcher));
                if (value_catcher != NULL) {
                    printf("Got this: %s\n", value_catcher);
                    free(value_catcher);
                }
                break;
            case 6:
                catch_error(close_connection());
                break;
            case 9:
                catch_error(close_connection());
                printf("Thank you for testing\n");
                return;
            default:
                printf("Wrong command\n");
                break;
        }
    }
}


int main(int argc, const char * argv[]) {
    UI();
    return 0;
}

