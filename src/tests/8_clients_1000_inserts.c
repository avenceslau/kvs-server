#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "KVS-lib.h"
#include "errors.h"

#define SIZE 24

#define NUMBER_OF_INSERTS 10

char* gen_secret(void){
    int aux;
    char* pw = (char*) malloc(SIZE+1);
    for(int i = 0; i < SIZE;i++){
        switch(rand()%3){
            case(0):
                aux = rand()%9 + 48;
                break;
            case(1):
                aux = rand()%25 + 65;
                break;
            default:
                aux = rand()%25 + 97;
                break;
        }
        pw[i] = (char) aux;
    }
    pw[SIZE] = '\0';
    return pw;
}

void f1(char * str){
    printf("------------Pid: %d changed %s\n-----------------------", getpid(), str);
}

int main(int argc, const char * argv[]) {
    char * val = NULL;
    int i = 0;
    int error = 0;
    int counter = 0;
    srand((unsigned) time(NULL));
    int inserted = 0;
    fork();
    fork();
    sleep(rand()%3);
    catch_error(establish_connection("group1", (char *) argv[1]));
    char *keys[NUMBER_OF_INSERTS], *values[NUMBER_OF_INSERTS];
    for( i = 0; i<NUMBER_OF_INSERTS; i++){
        keys[i] = gen_secret();
        values[i] = gen_secret();
        catch_error((error = put_value(keys[i],values[i])));
        free(values[i]);
        catch_error((error = register_callback(keys[i], f1)));
        values[i] = gen_secret();
        catch_error((error = put_value(keys[i],values[i])));
        catch_error((error = get_value(keys[i],&val)));
        if (val != NULL) {
            free(val);
            counter++;
            val = NULL;
        }
        catch_error((error = delete_key(keys[i])));
    }
    
    printf("%d, Inserted: %d retrieved %d\n", getpid(), inserted, counter);
    catch_error(close_connection());
    for( i = 0; i<NUMBER_OF_INSERTS; i++){
        if (val != NULL) {
            free(keys[i]);
            free(values[i]);
        }
    }
    return 0;
}
