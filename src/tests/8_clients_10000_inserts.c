#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "KVS-lib.h"
#include "errors.h"

#define SIZE 24

#define NUMBER_OF_INSERTS 5000

int n_callbacks = 0;

char* gen_secret(void){
    int aux;
    char* pw = (char*) malloc(SIZE+1);
    for(int i = 0; i < SIZE;i++){
        switch(rand()%3){
            case(0):
                aux = rand()%9 + 48;
                break;
            case(1):
                aux = rand()%25 + 65;
                break;
            default:
                aux = rand()%25 + 97;
                break;
        }
        pw[i] = (char) aux;
    }
    pw[SIZE] = '\0';
    return pw;
}

void f1(char * str){
    n_callbacks++;
}

int main(int argc, const char * argv[]) {
    char * val = NULL;
    int i = 0;
    int error = 0;
    int counter = 0;
    
    int inserted = 0;
    int deleted = 0;
    int overwritten = 0;
//    fork();
//    fork();
    srand((unsigned) time(NULL));
    sleep(3);
    catch_error((error = establish_connection("group1", (char *) argv[1])));
    
    if (error >= 0) {
        char *keys[NUMBER_OF_INSERTS], *values[NUMBER_OF_INSERTS];
        
        printf("Inserting Values\n");
        for( i = 0; i<NUMBER_OF_INSERTS; i++){
            keys[i] = gen_secret();
            values[i] = gen_secret();
            put_value(keys[i],values[i]);
            if (error >= 0) {
                inserted++;
            }
            
            if (values[i] != NULL) {
                free(values[i]);
                values[i] = NULL;
            }
        }
        
        printf("Register Callbacks\n");
        for( i = 0; i<NUMBER_OF_INSERTS; i++){
            register_callback(keys[i], f1);
        }
        
        printf("Overwriting Values\n");
        for( i = 0; i<NUMBER_OF_INSERTS; i++){
            values[i] = gen_secret();
            put_value(keys[i],values[i]);
            if (error >= 0) {
                overwritten++;
            }
            
            if (values[i] != NULL) {
                free(values[i]);
                values[i] = NULL;
            }
        }
        
        
        printf("Deleting Values\n");
        for (int i = 0 ; i < NUMBER_OF_INSERTS; i++) {
            delete_key(keys[i]);
            if (error >= 0) {
                deleted++;
            }
        }
        
        printf("Inserting Values\n");
        for( i = 0; i<NUMBER_OF_INSERTS; i++){
            keys[i] = gen_secret();
            values[i] = gen_secret();
            put_value(keys[i],values[i]);
            if (error >= 0) {
                inserted++;
            }
            if (values[i] != NULL) {
                free(values[i]);
            }
        }
        
        printf("Getting Values\n");
        for( i = 0; i<NUMBER_OF_INSERTS; i++){
            get_value(keys[i],&val);
            if (val != NULL) {
                free(val);
                counter++;
                val = NULL;
            }
        }
        printf("Deleting Values\n");
        for (int i = 0 ; i < NUMBER_OF_INSERTS; i++) {
            
            delete_key(keys[i]);
            if (error >= 0) {
                deleted++;
            }
        }
        printf("Process: %d\n\tInserted: %d\n\tRetrieved: %d\n\tOverwritten: %d\n\tDeleted: %d\n\tCallbacks received: %d\n", getpid(), inserted, counter, overwritten, deleted, n_callbacks);
        catch_error(close_connection());
    }
    return 0;
}

