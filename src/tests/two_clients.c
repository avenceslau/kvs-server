#include <unistd.h>
#include <stdio.h>
#include "KVS-lib.h"
#include "errors.h"

void f1(char * s){
    printf("The %s was changed\n", s);
}

int main(int argc, const char * argv[]) {
    char * key;
    char * val;
    int child;
    child = fork();
    catch_error(establish_connection("group1", (char *) argv[1]));
    
    if(child){
        catch_error(put_value("key1","val1"));
        catch_error(register_callback("key1", f1));
    }
    else{
        sleep(2);
        catch_error(put_value("key1","valchange"));
    }

    return 0;
}
