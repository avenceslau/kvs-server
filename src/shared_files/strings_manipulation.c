//
//  strings_manipulation.c
//  clent
//
//  Created by André Venceslau on 13/05/2021.
//

#include <string.h>
#include <stdlib.h>
#include "strings_manipulation.h"
#include "errors.h"
/**
 * @brief fuses two strings, returns the fused string, and returns as argument the length of the fused string
 *
 * @param str1 string 1 to be fused
 * @param str2 string 2 to be fused
 * @param len length of the fused strings passed as reference
 * @return fused string
 */
char *strfuse(char *str1, char *str2, int * len) {

    int len1 = (int) strlen(str1) + 1;
    int len2 = (int) strlen(str2) + 1;
    char *fused;
    int count = 0;
    (*len) = len1 + len2;
    fused = (char *)malloc((*len));
    if (fused == NULL) {
        return NULL;
    }
    for (int i = 0; i < len1; i++) {
        fused[count] = str1[i];
        count++;
    }
    for (int i = 0; i < len2; i++) {
        fused[count] = str2[i];
        count++;
    }
    return fused;
}

/**
 * @brief strcpy() that returns error
 *
 * @param dest string destination passed as reference
 * @param src string source
 * @return 0 or error
 */
int s_strcpy(char ** dest, char *src){

    size_t len = strlen(src) +1;
    if((*dest = (char *) malloc(len))){
        return FAILED_MALLOC;
    }

    strcpy(*dest, src);
    if ((strlen((*dest))+1) != len)
        return COPY_ERROR;
    
    return 0;
}

/**
 * @brief strcat() that returns error
 *
 * @param dest string destination passed as reference
 * @param src string that is concatenated to the destination string
 * @return 0 or error
 */
int s_strcat(char ** dest, char * src){
    int src_len = 0;
    int dest_len = 0;
    if (dest != NULL)
        dest_len = (int) strlen((*dest)) + 1;
    if (src != NULL)
        src_len = (int) strlen(src)+ 1;

    char * new_str = (char *) malloc((dest_len + src_len-1));
    if (new_str == NULL) {
        return FAILED_MALLOC;
    }
    strcpy(new_str, (*dest));
    strcat(new_str, src);
    free(*dest);
    (*dest) = new_str;
    if (strlen(new_str) != (dest_len + src_len-2))
        return CAT_ERROR;
    return 0;
}

/**
 * @brief converts an integer to string
 * 
 * @param number int that is to be converted to string
 * @return char* returns the string
 */
char * itos(int number){
    int tmp_number = number;
    int number_of_chars;
    for (number_of_chars = 0; tmp_number != 0 ; tmp_number/=10) {
        number_of_chars++;
    }
    char * str = (char *) malloc(number_of_chars+1);
    if(str == NULL){
        return str;
    }
    sprintf(str, "%d", number);
    str[number_of_chars] = '\0';
    return str;
}

/**
 * @brief does the malloc() of a string and returns error
 *
 * @param result allocated string passed as reference
 * @param str string
 * @return 0 or error
 */
int alloc_string(char ** result, const char * str){
    
    *result = (char *) malloc(strlen(str)+1);
    if (result == NULL) {
        return FAILED_MALLOC;
    }
    strcpy(*result, str);
    return 0;
}
