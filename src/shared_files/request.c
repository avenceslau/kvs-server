//
//  request.c
//  clent
//
//  Created by André Venceslau on 13/05/2021.
//

#include <stdio.h>
#include "request.h"
#include <stdlib.h>
#include <string.h>
#include "errors.h"
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>



int nsleep(long microseconds);

void print_request_ls(request_ls *req);
request_ls create_request_ls(int request_type, int n_elements_in_array);
int send_request_ls(request_ls req, char * str1, char * str2, int sock);
int process_request_ls(request_ls req, int socket, void ** res);

void free_request_as(request_as * request);
int process_request_as(char * request_str, request_as * req);
request_as unfuse_request(char request_str[BUFFER_SIZE]);
int create_resquest_as(char * request_type, char request[BUFFER_SIZE],  char * group_id, char * group_secret);


int nsleep(long microseconds) {
   struct timespec req, rem;

    req.tv_sec = 0;
    req.tv_nsec = microseconds * 1000;
   return nanosleep(&req , &rem);
}


/**
 * @brief prints the request
 * 
 * @param req structure containing the request
 */
void print_request_ls(request_ls *req){
    printf("\nClient requesting: ");
    switch (req->request_type) {
        case CONNECTION_REQUEST:
            printf("Connection\n");
            break;
        case PUT_VALUE:
            printf("Insertion\n");
            break;
        case GET_VALUE:
            printf("Value\n");
            break;
        case DELETE_VALUE:
            printf("Delete\n");
            break;
        case REGISTER_CALLBACK:
            printf("Register Callback\n");
            break;
        case CLOSE_REQUEST:
            printf("Close Connection Request\n");
            break;
        default:
            printf("ERROR INVALID_REQUEST\n");
            break;
    }
}

/**
 * @brief Create a request object
 * 
 * @param request_type type of request
 * @param n_elements_in_array number of elemntes
 * @return request 
 */
request_ls create_request_ls(int request_type, int n_elements_in_array){
    request_ls req;
    req.request_type = request_type;
    req.n_elements_array = n_elements_in_array;
    req.client_pid = getpid();
    return req;
}

/**
 * @brief sends a request to a socket
 * 
 * @param req request description
 * @param str1 string to be sent to the socket has part of the request
 * @param str2 string to be sent to the socket has part of the request
 * @param sock socket file desciptor to where the request will be sent
 * @return int returns 0 on a success on failed returns the error associated with it
 */
int send_request_ls(request_ls req, char * str1, char * str2, int sock) {
    int len;
    int len2;
    if (req.n_elements_array == 0) {
        if(write(sock, &req, sizeof(request_ls)) < 0)
            return WRITE_ERROR;
        return 0;
    }
    if (req.n_elements_array == 1) {
        if(write(sock, &req, sizeof(request_ls)) < 0)
            return WRITE_ERROR;
        len = (int) strlen(str1)+1;
        if(write(sock, &len, sizeof(int)) < 0)
            return WRITE_ERROR;
        if(write(sock, str1, len) < 0)
            return WRITE_ERROR;
        return 0;
    }
    
    if(write(sock, &req, sizeof(request_ls)) < 0)
        return WRITE_ERROR;
        
    len = (int) strlen(str1)+1;
    if(write(sock, &len, sizeof(int)) < 0)
        return WRITE_ERROR;
    len2 = (int) strlen(str2)+1;
    if(write(sock, &len2, sizeof(int)) < 0)
        return WRITE_ERROR;
    if(write(sock, str1, len) < 0)
        return WRITE_ERROR;
    
    if(write(sock, str2, len2) < 0)
        return WRITE_ERROR;
    return 0;
}

/**
 * @brief processes a request received according to the information contained in the req variable returns any strings read through res
 * 
 * @param req request
 * @param socket socket from which the request is to be read
 * @param res returns strings sent with the request
 * @return int 0 in a success and on a fail returns READ_ERROR or FAILED_MALLOC
 */
int process_request_ls(request_ls req, int socket, void ** res) {
    *res = NULL;
    if (req.n_elements_array == 0) {
        return 0;
    }
    if (req.n_elements_array == 1) {
        int size;
        char * str;
        if (read(socket, &size, sizeof(int)) < 0) {
            return READ_ERROR;
        }
        if ((str = (char *) malloc(size)) == NULL) {
            return FAILED_MALLOC;
        }
        if (read(socket, str, size) < 0) {
            free(str);
            return READ_ERROR;
        }
        *res = str;
        return 0;
    }
    
    int * size_of_strings = (int *) malloc(sizeof(int) * req.n_elements_array);
    if (size_of_strings == NULL) {
        return FAILED_MALLOC;
    }
    
    char ** str_array = (char **) malloc(sizeof(char *) * req.n_elements_array );
    if (size_of_strings == NULL) {
        return FAILED_MALLOC;
    }
    
    for (int i = 0; i < req.n_elements_array; i++) {
        read(socket, &size_of_strings[i], sizeof(int));
    }
    
    for (int i = 0; i < req.n_elements_array ; i++) {
        if((str_array[i] = (char *) malloc(size_of_strings[i])) == NULL){
            return FAILED_MALLOC;
        }
        if(read(socket, str_array[i], size_of_strings[i]) < 0) {
            for (int j = 0; j < i; j++) {
                free(str_array[j]); // WRONG VARIABLE WAS BEING USED
            }
            free(str_array);
            free(size_of_strings);
            return READ_ERROR;
        }
    }
    free(size_of_strings);
    *res = str_array;
    return 1;
}

void free_request_as(request_as * request) {
    if (request == NULL) {
        return;
    }
    if (request->request_type != NULL) {
        free(request->request_type);
        request->request_type = NULL;
    }
    if (request->group != NULL) {
        free(request->group);
        request->group = NULL;
    }
    if (request->secret != NULL) {
        free(request->secret);
        request->secret = NULL;
    }
}

/**
 * @brief Receives a request as a string and returns the request as request_as

 *  The string received contains the request structure values separated by '\0'

 * @param request_str request string
 * @return request_as request as a request_as structure
 */
request_as unfuse_request(char request_str[BUFFER_SIZE]){
    request_as req;
    size_t size_s;
    
    req.secret = NULL;
    req.group = NULL;
    char * code = strtok(request_str, "\0");
    char * group = strtok(request_str + strlen(code)+1, "\0");
    if (group == NULL) {
        return req;
    }
    
    if((size_s = strlen(code)+1) != SIZE_OF_CODE) {
        return req;
    }
    if((size_s = strlen(group)) == 0 || size_s > GROUPID_SIZE) {
        return req;
    }
    req.request_type = (char *) malloc(SIZE_OF_CODE);
    if (req.request_type == NULL) {
        return req;
    }
    strcpy(req.request_type, code);
    
    req.group = (char *) malloc(size_s+1);
    if (req.group == NULL) {
        free_request_as(&req);
        return req;
    }
    strcpy(req.group, group);
    
    if(strcmp(code, NEW_GROUP_AS) == 0){
        char * secret = strtok(request_str + strlen(code)+1 + strlen(group)+1, "\0");
        if ((size_s = strlen(secret)) == 0 || size_s > SECRET_SIZE) {
            free_request_as(&req);
            return req;
        }
        req.secret = (char *) malloc(size_s+1);
        if (req.secret == NULL) {
            free_request_as(&req);
            return req;
        }
        strcpy(req.secret, secret);
        return req;
    }
    
    return req;
}

int process_request_as(char * request_str, request_as * req) {
    (*req) = unfuse_request(request_str);
    if (req->request_type == NULL) {
        return INVALID_REQUEST;
    }
    if (req->group == NULL) {
        return NO_GROUP;
    }
    if (strcmp(req->request_type, NEW_GROUP_AS) == 0) {
        if (req->secret == NULL) {
            return ERROR_IN_SECRET;
        }
    }
    return 0;
}


int create_resquest_as(char * request_type, char request[BUFFER_SIZE],  char * group_id, char * group_secret){
    if (request_type == NULL) {
        return INVALID_REQUEST;
    }
    if (group_id == NULL) {
        return NO_GROUP;
    }
    
    if (strcmp(request_type, NEW_GROUP_AS) == 0) {
        if (group_secret == NULL) {
            return NO_SECRET;
        }
        
        strcpy(request, NEW_GROUP_AS);
        size_t resp_index = strlen(request)+1;
        
        for (size_t i = 0 ; i < strlen(group_id)+1; i++) {
            request[resp_index + i] = group_id[i];
        }
        resp_index += strlen(group_id)+1;
        for (size_t i = 0 ; i < strlen(group_secret)+1; i++) {
            request[resp_index + i] = group_secret[i];
        }
        return 0;
    }
    if (strcmp(request_type, DELETE_GROUP_AS) == 0) {
        strcpy(request, DELETE_GROUP_AS);
        size_t resp_index = strlen(request)+1;
        for (size_t i = 0 ; i < strlen(group_id)+1; i++) {
            request[resp_index + i] = group_id[i];
        }
        
        return 0;
    }
    if (strcmp(request_type, SEND_PASSWORD_AS) == 0) {
        strcpy(request, SEND_PASSWORD_AS);
        size_t resp_index = strlen(request)+1;
        for (size_t i = 0 ; i < strlen(group_id)+1; i++) {
            request[resp_index + i] = group_id[i];
        }
        return 0;
    }
    
    return 0;
}


int send_request_as(int auth_socket, char * request, struct sockaddr_in auth_addr, socklen_t auth_addr_size) {
    int werror;
    werror = (int) sendto(auth_socket, request, BUFFER_SIZE, 0, (const struct sockaddr *)&auth_addr, sizeof(auth_addr));
    return werror;
}

