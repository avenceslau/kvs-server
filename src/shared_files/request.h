#ifndef request_h
#define request_h

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>
#include "list.h"

#define MAXSLEEP 128

#define CONNECTION_REQUEST 10
#define DELETE_VALUE 20
#define GET_VALUE 30
#define PUT_VALUE 40
#define REGISTER_CALLBACK 50
#define CLOSE_REQUEST 60

#define NEW_GROUP_AS "1\0"
#define DELETE_GROUP_AS "2\0"
#define SEND_PASSWORD_AS "3\0"
#define INVALID_REQUEST_AS "0\0"

#define SIZE_OF_CODE 2
#define BUFFER_SIZE 128
#define GROUPID_SIZE 50
#define SECRET_SIZE 24

int nsleep(long microseconds);

typedef struct kvs{
    char * value;
    list * callback;
    size_t str_size;
} kvs;

typedef struct{
    int request_type;
    int n_elements_array;
    pid_t client_pid;
} request_ls;

int process_request_ls(request_ls req, int socket, void ** res);
void print_request_ls(request_ls * req);
request_ls create_request_ls(int request_type, int n_elements_in_array);
int send_request_ls(request_ls req, char * str1, char * str2, int sock);

#define NO_RESPONSE 1000
#define CONNECTION_ACCEPTED 100
#define CONNECTION_DENIED 200


typedef struct{
    char * request_type;
    char * group;
    char * secret;
} request_as;

int create_resquest_as(char * request_type, char request[BUFFER_SIZE],  char * group_id, char * group_secret);
int process_request_as(char * request_str, request_as * req);
int send_request_as(int auth_socket, char * request, struct sockaddr_in auth_addr, socklen_t auth_addr_size);




#endif /* request_h */
