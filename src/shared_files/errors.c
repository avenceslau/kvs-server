#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <string.h>
#include "errors.h"

void catch_error(int er) {
    switch(er){
        case AUTH_OFFLINE:
            printf("Authorization server is offline / could not connect\n");
            break;
        case CAT_ERROR:
            printf("String concatenation failed\n");
            break;
        case CLIENT_ERROR:
            printf("Could not write to client\n");
            break;
        case CONNECTION_FAILED:
            printf("Could not connect to server\n");
            break;
        case COPY_ERROR:
            printf("Could not copy string\n");
            break;
        case ERROR_IN_SECRET:
            printf("Could not bind\n");
            break;
        case FAILED_BIND:
            printf("FAILED_BIND\n");
            break;
        case FAILED_INSERT:
            printf("Could not insert value in the hashtable\n");
            break;
        case FAILED_ITEM_CREATION:
            printf("Could not create item\n");
            break;
        case FAILED_MALLOC:
            printf("Could not malloc\n");
            break;
        case FAILED_RESIZE:
            printf("Could not resize the hashtable\n");
            break;
        case FAILED_REGISTER_CALLBACK:
            printf("Could not register the callback\n");
            break;
        case GROUP_NOT_FOUND:
            printf("Group was not found\n");
            break;
        case GROUP_ALREADY_EXISTS:
            printf("Trying to create a group that already exists\n");
            break;
        case GROUPS_TABLE_ALREADY_EXISTS:
            printf("Trying to create Groups hashtable that already exists\n");
            break;
        case GROUPS_TABLE_DOESNT_EXIST:
            printf("Groups hashtable does not exist\n");
            break;
        case HT_CREATION_FAILED:
            printf("Could not create hashtable\n");
            break;
        case HASHTABLE_NOT_FOUND:
            printf("Hash table not found\n");
            break;
        case HASHTABLE_FULL:
            printf("Hashtable full (could not resize)\n");
            break;
        case INVALID_REQUEST:
            printf("Invalid request\n");
            break;
        case INVALID_FUNCTION:
            printf("Callback function is wrong\n");
            break;
        case KEY_ERROR:
            printf("Key error\n");
            break;
        case KEY_HAS_NOVALUE:
            printf("Key has no value\n");
            break;
        case KEY_NOT_FOUND:
            printf("Key not found\n");
            break;
        case PTHREAD_CREATION_FAILED:
            printf("Thread could not be created\n");
            break;
        case PTHREAD_RD_LOCK_INIT_FAILED:
            printf("Could not initialize rw lock mutex\n");
            break;
        case MUTEX_INIT_FAILED:
            printf("Could not initialize mutex\n");
            break;
        case NO_ITEM:
            printf("No item was passed\n");
            break;
        case NO_GROUP:
            printf("No group was passed\n");
            break;
        case NO_SECRET:
            printf("No secret was passed\n");
            break;
        case READ_ERROR:
            printf("Read error\n");
            break;
        case RESIZING:
            printf("Could not resize the hashtable (memory full)\n");
            break;
        case SERVER_NOT_FOUND:
            printf("Server not found\n");
            break;
        case SOCK_CREATION_FAILED:
            printf("Could not create socket\n");
            break;
        case VALUE_ERROR:
            printf("No value was passed\n");
            break;
        case WRITE_ERROR:
            printf("Not connected, no commuication channel found\n");
            break;
        case WRONG_NUMBER_OF_ARGUMENTS:
            printf("Wrong number of arguments\n");
            break;
        case WRONG_SECRET:
            printf("Secret was incorrect\n");
            break;
        case UNKNOWN:
            printf("Unknown error\n");
            break;
        default:
            break;
    }
}

void handle_error(int error, void solve_error(int)){
    solve_error(error);
}
