//
//  strings_manipulation.h
//  clent
//
//  Created by André Venceslau on 13/05/2021.
//

#ifndef strings_manipulation_h
#define strings_manipulation_h

#include <stdio.h>

char *strfuse(char *str1, char *str2, int * len);
int s_strcpy(char ** dest, char *src);
int s_strcat(char ** dest, char * src);
char * itos(int number);
int alloc_string(char ** result, const char * str);

#endif /* strings_manipulation_h */
