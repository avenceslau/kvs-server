//
//  response.h
//  auth_server
//
//  Created by André Venceslau on 03/06/2021.
//

#ifndef response_h
#define response_h

#include <stdio.h>
#include <netinet/in.h>
#include "request.h"

#define REQUEST_UNFULFILLED "-5\0"
#define REQUEST_FULFILLED_W_STR "+2\0"
#define REQUEST_FULFILLED_N_STR "+1\0"
#define SIZE_RESP_CODE 3

typedef struct {
    bool is_string;
    size_t str_size;
    int error;
} response_ls; //querie result

int send_error_response_ls(int error, int sock);
int send_response_ls(int error, kvs * str, int sock);
int read_response_ls(int socket, char **res);


typedef struct{
    char * response_result;
    char * secret;
} response_as;

response_as unfuse_response_as(char * response);
int create_response_as(char * request_type, char response[BUFFER_SIZE], char * value);
int send_error_response_as(int error, int auth_socket, struct sockaddr_in client_addr, socklen_t  client_addr_size);
int send_response_as(int auth_socket, char * response, struct sockaddr_in client_addr, socklen_t client_addr_size);
int read_response_as(int auth_socket, char response[BUFFER_SIZE]);

#endif /* response_h */
