//
//  response.c
//  auth_server
//
//  Created by André Venceslau on 03/06/2021.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "errors.h"
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include "response.h"
#include "errors.h"

response_ls create_response(bool is_string, size_t str_size, int error);
int send_error_response_ls(int error, int sock);
int send_response_ls(int error, kvs * str, int sock);
int read_response_ls(int socket, char **res) ;


int create_response_as(char * request_type, char response[BUFFER_SIZE], char * value);
int send_error_response_as(int error, int auth_socket, struct sockaddr_in client_addr, socklen_t client_addr_size);
int send_response_as(int auth_socket, char * response, struct sockaddr_in client_addr, socklen_t client_addr_size);
response_as unfuse_response_as(char * response);
int read_response_as(int auth_socket, char response[BUFFER_SIZE]);



/**
 * @brief Create a response object
 *
 * @param is_string is true if the response has a string
 * @param str_size size of the string if it exists
 * @param error should have the response status
 * @return response_r returns the response object created
 */
response_ls create_response(bool is_string, size_t str_size, int error) {
    response_ls resp;
    resp.is_string = is_string;
    resp.str_size = str_size;
    resp.error = error;
    if (is_string == false) {
        resp.str_size = 0;
    }
    return resp;
}

/**
 * @brief sends an error response
 *
 * @param error error code
 * @param sock socket to where the response will be sent
 * @return int returns 0 on a success on a fail returns CLIENR_ERROR
 */
int send_error_response_ls(int error, int sock) {
    response_ls resp = create_response(false, 0, error);
    if(write(sock, &resp, sizeof(response_ls)) < 0)
        return CLIENT_ERROR;
    return 0;
}

/**
 * @brief sends a response to a connected socket
 *
 * @param error any error to be associated with the response
 * @param str string to be sent as response
 * @param sock socket to where the response will be sent
 * @return int
 */
int send_response_ls(int error, kvs * str, int sock) {
    response_ls resp;
    
    if (str == NULL) {
        resp = create_response(false, 0, error);
        if(write(sock, &resp, sizeof(response_ls)) < 0)
            return CLIENT_ERROR;
        return 0;
    }
    if (str->value == NULL) {
        resp = create_response(false, 0, error);
        if(write(sock, &resp, sizeof(response_ls)) < 0)
            return CLIENT_ERROR;
        return KEY_HAS_NOVALUE;
    }
    resp = create_response(true, str->str_size, error);
    if(write(sock, &resp, sizeof(response_ls)) < 0)
        return CLIENT_ERROR;
    if(write(sock, str->value, resp.str_size) < 0)
        return CLIENT_ERROR;
    return 0;
}

/**
 * @brief reads a response from a socket
 *
 * @param socket socket from where the response will be read
 * @param res any string returned by the response is returned through res
 * @return int returns the error of associated with the response, which has 0 if there is no error
 */
int read_response_ls(int socket, char **res) {
    response_ls resp;
    
    if (read(socket, &resp, sizeof(response_ls)) < 0) {
        return READ_ERROR;
    }
    if (resp.error < 0) {
        return resp.error;
    }
    if (resp.is_string) {
        if ((*res = (char *) malloc(resp.str_size)) == NULL) {
            return FAILED_MALLOC;
        }
        if (read(socket, *res, resp.str_size) < 0){
            free(*res);
            return READ_ERROR;
        }
    }
    return resp.error;
}

/**
 * @brief Sends an error response to local-server from the auth-server
 *
 * @param error error to send
 * @param auth_socket auth-server socket
 * @param client_addr local-server address
 * @param client_addr_size local-server adress size
 * @return number of bytes sent or error
 */
int send_error_response_as(int error, int auth_socket, struct sockaddr_in client_addr, socklen_t client_addr_size) {
    int werror;
    werror = (int) sendto(auth_socket, REQUEST_UNFULFILLED, BUFFER_SIZE, 0, (const struct sockaddr *)&client_addr, client_addr_size);
    return werror;
}

/**
 * @brief Create a response_as
 *
 * @param request_type type of reuest
 * @param response value of the response
 * @param value value of the response
 * @return int
 */
int create_response_as(char * request_type, char response[BUFFER_SIZE], char * value){
    if (request_type == NULL) {
        return NO_REQUEST_TYPE;
    }
    
    if (strcmp(request_type, NEW_GROUP_AS)==0) {
        strcpy(response, REQUEST_FULFILLED_N_STR);
        return 0;
    }
    if (strcmp(request_type, DELETE_GROUP_AS)==0) {
        strcpy(response, REQUEST_FULFILLED_N_STR);
        return 0;
    }
    if (strcmp(request_type, SEND_PASSWORD_AS)==0) {
        strcpy(response, REQUEST_FULFILLED_W_STR);
        size_t resp_index = strlen(response)+1;
        if (value == NULL) {
            return VALUE_ERROR;
        }
        for (size_t i = 0 ; i < strlen(value)+1; i++) {
            response[resp_index + i] = value[i];
        }
        return 0;
    }
    
    return 0;
}


void free_response_as(response_as res){
    if (res.secret != NULL) {
        free(res.secret);
    }
    if (res.response_result == NULL) {
        free(res.response_result);
    }
    return;
}

int read_response_as(int auth_socket, char response[BUFFER_SIZE]){
    return (int) recv(auth_socket, response, BUFFER_SIZE, 0);
}

/**
 * @brief Receives a response as a string and returns the response as response_as

 *  The string received contains the response structure values separated by '\0'

 * @param response response string
 * @return response_as response as a response_as structure
 */
response_as unfuse_response_as(char * response){
    response_as res;
    size_t size_s;
    
    res.secret = NULL;
    char * code = strtok(response, "\0");
    
    if((size_s = strlen(code)+1) != SIZE_RESP_CODE) {
        return res;
    }
    
    res.response_result = (char *) malloc(SIZE_RESP_CODE);
    if (res.response_result == NULL) {
        return res;
    }
    strcpy(res.response_result, code);
    
    if(strcmp(code, REQUEST_FULFILLED_W_STR) == 0){
        char * secret = strtok(response + strlen(code)+1, "\0");
        if ((size_s = strlen(secret)) == 0 || size_s > SECRET_SIZE) {
            free_response_as(res);
            return res;
        }
        res.secret = (char *) malloc(size_s);
        if (res.secret == NULL) {
            free_response_as(res);
            return res;
        }
        strcpy(res.secret, secret);
        return res;
    }
    
    return res;
}

/**
 * @brief sends a response to the auth-server
 *
 * @param auth_socket socket to auth-server
 * @param response response given by local server
 * @param client_addr address to local server
 * @param client_addr_size size of the address to the local-server
 * @return number of bytes sent or -1 if error
 */
int send_response_as(int auth_socket, char * response, struct sockaddr_in client_addr, socklen_t client_addr_size){
    int werror;
    werror = (int) sendto(auth_socket, response, BUFFER_SIZE, 0, (const struct sockaddr *)&client_addr, sizeof(client_addr));
    return werror;
}
