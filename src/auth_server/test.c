#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <string.h>
#include <time.h>



#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

void main(int argc, const char * argv[]){

    struct sockaddr_in server_addr;
    char buff[100];
    int nbytes;
    char linha[1000];
    char linha1[1000];
    struct sockaddr_in local_addr;

    if(argc<2){
        printf("Remoted address required\n");
        exit(-1);
    }
    int sock_fd= socket(AF_INET, SOCK_DGRAM, 0);
 
    if(sock_fd == -1){
        perror("socket\n");
        exit(-1);
    }

    local_addr.sin_family = AF_INET;
    local_addr.sin_addr.s_addr = INADDR_ANY;
    local_addr.sin_port = htons(3000);
    int err = bind(sock_fd, (struct sockaddr *) &local_addr, sizeof(local_addr));
    if(err == -1){
        perror("bind");
        exit(-1);
    }
    printf("socket created \n Ready to send\n");

    server_addr.sin_family = AF_INET;
    inet_aton(argv[1], &server_addr.sin_addr);
    server_addr.sin_port = htons(3001);
    do{
        printf("Write message\n");
        fgets(linha,1000,stdin);
        linha[strlen(linha)-1] = '\0';
        nbytes = sendto(sock_fd, linha, strlen(linha)+1, 0, (const struct sockaddr *)&server_addr, sizeof(server_addr));
        printf("sent %d bytes %s", nbytes, linha);
        nbytes = recv(sock_fd,linha1,1000,0);
        printf("received %d bytes %s", nbytes, linha1);
    }while(nbytes > 0);

}