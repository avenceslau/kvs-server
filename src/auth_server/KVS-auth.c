//
//  main.c
//  server.c
//
//  Created by André Venceslau on 13/05/2021.
//

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "HASH-Table.h"
#include "errors.h"
#include "request.h"
#include "response.h"

/** 1 for type of request + 1 for termination char  + 50 for group size + 1 for termination char
 * + 24 for secret + 1 for terminaton char  = 78 > 64 round to next multiple of bytes 128 */
#define BUFF_SIZE 128
#define MAX_MESSAGE_SIZE 128

int add_group_ht(request_as request);
int solve_request(request_as request,char ** secret);
int create_server_socket(void);
void run_auth(void);

int auth_socket;
struct sockaddr_in server_addr;
hash_table * groups;

int add_group_ht(request_as request) {
    int error;
    void * value;
    if((error = ht_search(groups, request.group, &value)) == 0){
        return GROUP_ALREADY_EXISTS;
    }
    
    
    if((error = ht_insert(&groups, request.group, (char *) request.secret, free)) < 0){
        return error;
    }
    if (error == RESIZE_HT) {
        (void) ht_resize(&groups, free);
    }
    
    return 0;
}

int get_secret_ht(request_as request, char ** secret){
    int error;
    void * value;
    if((error = ht_search(groups, request.group, &value)) < 0){
        *secret = NULL;
        return error;
    }
    *secret = (char *) value;
    return 0;
}

int delete_group_secret(request_as request) {
    void * value;
    delete_key_return_value(groups, request.group, &value);
    if (value == NULL) {
        return KEY_NOT_FOUND;
    }
    free(value);
    return 0;
}

int solve_request(request_as request, char ** secret){
    *secret = NULL;
    int error;
    if (request.request_type == NULL) {
        return INVALID_REQUEST;
    }
    if (strcmp(request.request_type, NEW_GROUP_AS) == 0) {
        if((error = add_group_ht(request)) <0){
            return error;
        }
        return 0;
    }
    if (strcmp(request.request_type, SEND_PASSWORD_AS) == 0) {
        if((error = get_secret_ht(request,secret)) <0){
            return error;
        }
        return 0;
    }
    if (strcmp(request.request_type, DELETE_GROUP_AS) == 0) {
        if((error = delete_group_secret(request)) <0){
            return error;
        }
        return 0;
    }
    
    return INVALID_REQUEST;
}

int create_server_socket(void){
    auth_socket = socket(AF_INET, SOCK_DGRAM, 0);
    
    if (auth_socket == -1){
        return SOCK_CREATION_FAILED;
    }
    server_addr.sin_family = AF_INET;
    inet_aton("0.0.0.0", &server_addr.sin_addr);
    server_addr.sin_port = htons(3001); // host to network short, port number
    

    if(bind(auth_socket,(struct sockaddr *) &server_addr, sizeof(server_addr)) == -1){
        return FAILED_BIND;
    }
    printf("Socket created and binded\n");
    printf("Ready to receive messages\n");
    return 0;
}

void run_auth(void){
    int error = 0;
    char * secret = NULL;
    request_as req;
    if((error = create_server_socket()) < 0){
        catch_error(error);
        return;
    }
    if((error = create_table(&groups)) < 0){
        catch_error(error);
        return;
    }
    struct sockaddr_in client_addr;
    socklen_t size_client_addr;
    char request[BUFF_SIZE];
    char response[BUFF_SIZE];
    ssize_t n_bytes;
    
    while (1) {
        size_client_addr = sizeof(struct sockaddr_storage);
        n_bytes = recvfrom(auth_socket, request, BUFF_SIZE, 0, (struct sockaddr *) &client_addr, &size_client_addr);
        printf("Bytes read %d\n", (int) n_bytes);
        if (n_bytes > MAX_MESSAGE_SIZE) {
            error = send_error_response_as(error, auth_socket,client_addr, size_client_addr);
        }
        error = process_request_as(request, &req);
        if(error < 0)
            error = send_error_response_as(error, auth_socket, client_addr, size_client_addr);
        error = solve_request(req, &secret);
        if(error < 0)
            error = send_error_response_as(error, auth_socket, client_addr, size_client_addr);
        error = create_response_as(req.request_type, response, secret);
        if (error < 0)
            error = send_error_response_as(error, auth_socket, client_addr, size_client_addr);
        
        (void) send_response_as(auth_socket, response, client_addr, size_client_addr);
    }
}
