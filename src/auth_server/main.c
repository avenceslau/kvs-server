//
//  main.c
//  server.c
//
//  Created by André Venceslau on 13/05/2021.
//

#include <stdio.h>
#include "errors.h"
#include "KVS-auth.h"

int main(int argc, const char * argv[]) {
    
    run_auth();
    return 0;
}

