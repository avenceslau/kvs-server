#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <fcntl.h>

#include "request.h"
#include "HASH-Table.h"
#include "errors.h"
#include "kvs_server.h"
#include "group.h"
#include "strings_manipulation.h"
#include "list.h"
#include "response.h"

#define SILENCE 9999999999
#define MAX_TIME 30
#define MAX_CLIENTS 6
#define MAX_CHARACTERS 51

#define SOCKNAME "/tmp/local_server"
#define CALLBACK_SOCK "/tmp/callbacks"
#define AUTH_LOCATION "127.0.0.1"

void modify_client(void * client_item, void * not_used);
void disconnect_client_item(int client_pid);
void write_error_handler(int error, char * group_id, request_ls * req, int client_sock, clock_t * timestamp);
void * print_client(void * client);
int get_group_secret(char * group_id, char **secret);
int server_register_callback(char ** group, request_ls *req, int client_sock, clock_t * timestamp, int * client_callback_sock);
int check_secret(char * group_id, char * secret);
int close_client(char ** group_id, request_ls *req, int client_sock, clock_t * timestamp);
int server_put_value(char ** group_id, request_ls *req, int client_sock, clock_t * timestamp);
int server_get_value(char ** group_id, request_ls *req, int client_sock, clock_t * timestamp);
int server_delete_value(char ** group_id,request_ls *req, int client_sock, clock_t * timestamp);
int accept_request(char ** group, request_ls *req, int client_sock, clock_t * timestamp);
int check_request(request_ls * req, char ** group_id, int client_sock, clock_t * timestamp, int * client_callback_sock);
void print_user_options(void);
void read_group_id(char group_id[MAX_CHARACTERS]);
void UI(pthread_t t_id);
void  * create_server(void * args);
void run_server(void);
void * create_client(void * client);
void free_client(void * client);
int get_client_id(void * client);
void create_socket_to_auth(void);


//volatile sig_atomic_t exit_requested = 0;
int server_sock;
int auth_sock;
int callback_sock;

struct sockaddr_in auth_addr;
struct sockaddr_in local_server_addr;

pthread_mutex_t auth_lock;
pthread_mutex_t accept_callback_lock;
pthread_mutex_t list_lock;
groups_ht * groups;
list * connected_clients;
list * previously_connected_clients;

typedef struct {
    pid_t pid;
    clock_t timestamp;
    struct tm * end;
    struct tm * start;
} client_info;

typedef struct {
    int timestamp;
    char * key_to_watch;
    int client_fd;
    list * list;
} callback;

/**
 * @brief Creates a callback to a client
 *
 * @param client_callback callback structure
 * @return callback or null if the allocation failed
 */
void * create_callback(void * client_callback){
    callback * c_back = (callback *) malloc(sizeof(callback));
    if (c_back == NULL) {
        return NULL;
    }
    c_back->key_to_watch = (char *) malloc(strlen(((callback*) client_callback)->key_to_watch)+1);
    if (c_back->key_to_watch == NULL) {
        free(c_back);
    }
    strcpy(c_back->key_to_watch, ((callback*) client_callback)->key_to_watch);
    c_back->client_fd = ((callback *) client_callback)->client_fd;
    c_back->timestamp = ((callback *) client_callback)->timestamp;
    c_back->list = ((callback *) client_callback)->list;
    return c_back;
}

/**
 * @brief frees a callback to client
 *
 * @param c_back callback to be freed
 */
void free_callback(void * c_back){
    if (c_back == NULL) {
        return;
    }
    callback * aux = (callback *) c_back;
    if (aux->key_to_watch != NULL) {
        free(aux->key_to_watch);
    }
//    close(aux->client_fd); //WAS BREAKING FOR DELETE VALUE CLOSING FD TO SOON
    free(aux);
    return;
}

/**
 * @brief gets the timestamp of the callback that is used as an identifier
 *
 * @param c_back callback structure
 * @return timestamp of the callback
 */
int callback_get_key(void * c_back){
    return (int) ((callback *) c_back)->timestamp;
}

/**
 * @brief Sends a callback
 *
 * @param c_back callback structure
 * @return callback structure or null if the item refered in the callback does not exist
 */
void *send_callback(void * c_back){
    callback * aux = (callback *) c_back;
    void * exists;
    if (aux == NULL) {
        return NULL;
    }
    pthread_mutex_lock(&list_lock);
    exists = find_item(aux->timestamp, connected_clients);
    if (exists == NULL) {
        pthread_mutex_unlock(&list_lock);
        return NULL;
    }
    if(write(aux->client_fd, aux->key_to_watch, 25) <= 0) {
        int timestamp = (int) aux->timestamp;
        pthread_mutex_unlock(&list_lock);
        disconnect_client_item(timestamp);
        return c_back;
    }
    pthread_mutex_unlock(&list_lock);
    return c_back;
}

void modify_callback(void * callback1, void * callback2){
    return;
}


//void disconnect_server(pthread_t t_id){
//    printf("Server was killed\n");
//    shutdown(server_sock, SHUT_RD);
//    pthread_join(t_id, NULL);
//    free_table(groups_table);
//    unlink(SOCKNAME);
//    exit(0);
//
//}
//
//void sighandler(int signum){
//    exit_requested = 1;
//    return;
//}

/**
 * @brief Updates the end time of the client to the current time
 *
 * @param client_item to be updated
 */
void modify_client(void * client_item, void * not_used){
    if (client_item == NULL) {
        return;
    }
    time_t rawtime;
    time ( &rawtime );
    ((client_info *) client_item)->end = localtime (&rawtime);
}

/**
 * @brief Disconnects a client
          modifies the client end time to current time
          removes the client from the connected_clients list
          adds the client to the previously_connected_clients list
 *
 * @param timestamp identifier of the client
 */
void disconnect_client_item(int timestamp){
    void * aux;
    pthread_mutex_lock(&list_lock);
    modify_client(find_item(timestamp, connected_clients), NULL);
    aux = remove_item_from_list(timestamp, connected_clients);
    add_to_list(previously_connected_clients, aux);
    pthread_mutex_unlock(&list_lock);

    free(aux);
}

/**
 * @brief if a write to a client fails, 
 *        that client is disconnected and the client thread is closed
 * 
 * @param error error code
 * @param req request
 * @param group_id group_id
 */
void write_error_handler(int error, char * group_id, request_ls * req, int client_sock, clock_t * timestamp) {
    if (error == CLIENT_ERROR) {
        close_client(&group_id, req, client_sock, timestamp);
    }
    return;
}

/**
 * @brief Create a client object
 * 
 * @param client client item
 * @return void* client object casted to void *
 */
void * create_client(void * client) {
    client_info * aux = (client_info *) malloc(sizeof(client_info));
    if (aux == NULL) {
        return NULL;
    }
    aux->pid = (*((client_info *) client)).pid;
    aux->timestamp = (*((client_info *) client)).timestamp;
    aux->start = (*((client_info *) client)).start;
    aux->end = (*((client_info *) client)).end;
    return (void *) aux;
}

/**
 * @brief Free client object
 * 
 * @param client object to be freed
 */
void free_client(void * client) {
    free(client);
}

/**
 * @brief Get the client object id
 * 
 * @param client client object
 * @return int client id
 */
int get_client_id(void * client) {
    return (int) ((client_info *) client)->timestamp;
}

/**
 * @brief prints a client object
 * 
 * @param client object to be printed
 */
void * print_client(void * client) {
    if (client == NULL) {
        return NULL;
    }
    printf("\tClient pid: %d\n", (*((client_info *)client)).pid);
    if ((*((client_info *)client)).start != NULL) {
        printf("\tEstablished connection at: %s",asctime((*((client_info *)client)).start));
    }
    if ((*((client_info *)client)).end != NULL) {
        printf("\tConnection ended at: %s", asctime((*((client_info *)client)).end));
    }
    printf("\n");
    return client;
}

/**
 * @brief Create a socket to communicate with auth server
 * 
 */
void create_socket_to_auth(void) {
    auth_sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (auth_sock == -1) {
        perror("Socket to communicate with authentification server creation failed");
        exit(-1);
    }
    struct timeval tv;
    tv.tv_sec = 3;
    tv.tv_usec = 0;
    if (setsockopt(auth_sock, SOL_SOCKET, SO_RCVTIMEO, (const char *)&tv, sizeof(tv)) < 0) {
        perror("setsockopt");
        exit(-1);
    }
    local_server_addr.sin_family = AF_INET;
    local_server_addr.sin_addr.s_addr = INADDR_ANY;
    local_server_addr.sin_port = htons(3003);
    int err = bind(auth_sock, (struct sockaddr *) &local_server_addr, sizeof(local_server_addr));
    if(err == -1){
        perror("Socket binding failed");
        exit(-1);
    }
    auth_addr.sin_family = AF_INET;
    inet_aton(AUTH_LOCATION, &auth_addr.sin_addr);
    auth_addr.sin_port = htons(3001);
//    send_request_as(auth_sock, "1\0group1\0secret\0", auth_addr,sizeof(auth_addr));
}

/**
 * @brief Requests the group secret from the auth server 
 * 
 * @param group_id group id
 * @param secret the secret will be returned through here
 * @return int 0 on a success or an error
 */
int get_group_secret(char * group_id, char **secret) {
    char request[BUFFER_SIZE];
    char response[BUFFER_SIZE];
    int error;
    response_as res;
    *secret = NULL;
    
    create_resquest_as(SEND_PASSWORD_AS, request, group_id, NULL);
    
    pthread_mutex_lock(&auth_lock);
    error = send_request_as(auth_sock, request, auth_addr, sizeof(auth_addr));
    if(read_response_as(auth_sock,response) > 0) {
        pthread_mutex_unlock(&auth_lock);
        res = unfuse_response_as(response);
        if (res.response_result == NULL) {
            return AUTH_OFFLINE;
        }
        if (res.secret == NULL) {
            return GROUP_NOT_FOUND;
        }
        *secret = res.secret;
        return 0;
    }
    pthread_mutex_unlock(&auth_lock);
    
    return AUTH_OFFLINE;
}

/**
 * @brief registers a callback on request 
 *        from a client associated with a key
 * 
 * @param group_id name of the group
 * @param req request
 * @param client_sock socket to communicate with the client 
 * @return int 0 or error on a fail
 */
int server_register_callback(char ** group_id, request_ls *req, int client_sock, clock_t * timestamp, int * client_callback_sock) {
    void * request_res;
    int error;
    if (req->n_elements_array != 1) {
        error = send_error_response_ls(WRONG_NUMBER_OF_ARGUMENTS, client_sock);
        write_error_handler(error, *group_id, req, client_sock, timestamp);
        return WRONG_NUMBER_OF_ARGUMENTS;
    }
    error = process_request_ls((*req), client_sock, &request_res);
    if (error < 0) {
        error = send_error_response_ls(error, client_sock);
        write_error_handler(error, *group_id,req, client_sock, timestamp);
        return error;
    }

    char * key = (char *) request_res;
    void * value;
    void * group;
    /* read lock the groups table to check if the group still exists */
    pthread_rwlock_rdlock(&groups->lock);
    error = ht_search(groups->table, *group_id, &group);
    /* if the groups exists */
    if (error >= 0) {
        /* read lock the group */
        pthread_rwlock_wrlock(&((group_r *) group)->lock);
        /* unlock groups table*/
        pthread_rwlock_unlock(&groups->lock);
        /* find the value and send it to the client */
        error = ht_search(((group_r *) group)->g_table, key, &value);
        pthread_mutex_lock(&accept_callback_lock);
        if (error >= 0) {
            error = send_response_ls(error, (kvs *) value, client_sock);
        }
        if (error < 0) {
            error = send_error_response_ls(error, client_sock);
        }
        write_error_handler(error, *group_id,req, client_sock, timestamp);
        kvs * aux = (kvs *) value;
        callback client_callback;
        client_callback.timestamp = (int)*timestamp;
        if ((*client_callback_sock < 0) && (error >= 0)) { //&& (error >= 0) WAS MISSING WITHOUT DEADLOCKS
            *client_callback_sock = accept(callback_sock, NULL, NULL); //May cause deadlocks doesn´t have a timeout (should've had a timeout) (in hindsight the same thing could be accomplished by sending a signal to the client and than sending the key through the socket used for normal comunication + select function maybe)
            int flags = fcntl(*client_callback_sock, F_GETFL);
            error = fcntl(*client_callback_sock, F_SETFL, flags | O_NONBLOCK);
            
        }
        if (value != NULL) {
            if (((kvs *)value)->value != NULL) {
                client_callback.client_fd = *client_callback_sock;
                client_callback.list = aux->callback;
                client_callback.key_to_watch = key;
                add_to_list(aux->callback, &client_callback);
            }
        }
        pthread_mutex_unlock(&accept_callback_lock);
        /* unlock the group*/
        pthread_rwlock_unlock(&((group_r *) group)->lock);
        
    } else {
        /* if the group does't exist unlock groups table*/
        pthread_rwlock_unlock(&groups->lock);
    }

    return error;
}

/**
 * @brief verifies if client has the right secret 
 *        for the group_id to which they are trying to connect
 * 
 * @param group_id group that the client wants to connect
 * @param secret secret to access the group
 * @return int 0 on a success
 */
int check_secret(char * group_id, char * secret) {
    char request[BUFFER_SIZE];
    char response[BUFFER_SIZE];
    int error = 0;
    response_as res;

    create_resquest_as(SEND_PASSWORD_AS, request, group_id, NULL);
    pthread_mutex_lock(&auth_lock);
    error = send_request_as(auth_sock, request, auth_addr, sizeof(auth_addr));
    if(read_response_as(auth_sock, response) > 0) {
        pthread_mutex_unlock(&auth_lock);
        res = unfuse_response_as(response);
        if (res.response_result == NULL) {
            error AUTH_OFFLINE;
        }
        if (res.secret == NULL) {
            error = GROUP_NOT_FOUND;
        }
        if ((res.secret != NULL) && (strcmp(res.secret, secret) == 0)) {
            return 0;
        }
        if ((res.response_result != NULL) && (strcmp(res.response_result, REQUEST_UNFULFILLED) == 0)) {
            return GROUP_NOT_FOUND;
        }
        return UNKNOWN; //FIXME???????
    }
    pthread_mutex_unlock(&auth_lock);
    if (error < 0) {
        return error;
    }
    return AUTH_OFFLINE;
}

/**
 * @brief function that closes a client as requested by the client itself
 *        processes the request and checks its parameters
          disconnects the client identified by the timestamp
          sends a response to the client being removed
          closes the client socket
 * @param group_id identifier of the group
 * @param req request
 * @param client_sock socket of the client
 * @param timestamp identifier of the client
 * @return 1 on success or error
 */
int close_client(char ** group_id, request_ls *req, int client_sock, clock_t * timestamp) {
    void * request_res;
    int error;
    
    
    error = process_request_ls((*req), client_sock, &request_res);
    disconnect_client_item((int)*timestamp);
    error = send_response_ls(error, NULL, client_sock);
    close(client_sock);
    return error;
}

/**
 * @brief Puts a value in the provided key,to a provided group, as requested by a client

 *  checks if the parameters are correct, if not returns error
    processes the request given
    searches the groups table for the provided group
    if the group given exists, takes the callback list associated to the key-value
    if the callback list is not empty, for each member of the list calls the function send_callback
    inserts the value in the hashtable by the provided key
    if the insert returns error RESIZE_HT, the table is full and is resized by ht_resize


 * @param group_id group identifier
 * @param req request sent by the client
 * @param client_sock client socket
 * @param timestamp identifier of the client
 * @return 1 on success or error
 */
int server_put_value(char ** group_id, request_ls *req, int client_sock, clock_t * timestamp) {
    void * group;
    int error;
    list *l = NULL;
    if (req->n_elements_array != 2) {
        error = send_error_response_ls(WRONG_NUMBER_OF_ARGUMENTS, client_sock);
        write_error_handler(error, *group_id, req, client_sock, timestamp);
        return WRONG_NUMBER_OF_ARGUMENTS;
    }
    void * aux;
    kvs * value = (kvs *) malloc(sizeof(kvs));
    if (value == NULL) {
        return FAILED_MALLOC;
    }
    
    void * request_res;
    error = process_request_ls((*req), client_sock, &request_res);
    
    if (error < 0) {
        free(value);
        return error;
    }
    
    char * key = ((char **) request_res)[0];
    value->value = (void *) ((char **) request_res)[1];
    value->str_size = strlen(value->value) +1;
    value->callback = create_list(create_callback, free_callback, callback_get_key, send_callback, modify_callback);
    /*read lock groups table to see if the group still exists*/
    pthread_rwlock_rdlock(&groups->lock);
    error = ht_search(groups->table, *group_id, &group);

    /* if the group exists*/
    if (error >= 0) {
        /* write lock the group so that there are no readers if we need to rehash*/
        pthread_rwlock_wrlock(&((group_r *) group)->lock);
        /*unlock groups table*/
        pthread_rwlock_unlock(&groups->lock);
        pthread_mutex_lock(&accept_callback_lock); //WAS MISSPLACED
        /* insert the key */
        error = ht_search(((group_r *) group)->g_table, key, &aux);
        if (error == 0) {
            l = value->callback;
            value->callback = ((kvs *) aux)->callback;
            print_list(value->callback);

        }
        error = ht_insert(&((group_r *) group)->g_table, key , (void *) value, free_kvs_item);
        if (error == RESIZE_HT) {
            /*resizing only changes the size of the group table and rehashes the items*/
            /* there is no need to write lock the groups table*/
            /* we don't need to check the error on the resize if there is an error the hash table will remain full*/
            /* if there isn't an error than it has been ressized */
            (void) ht_resize(&((group_r *) group)->g_table, free_kvs_item);
        }
        pthread_mutex_unlock(&accept_callback_lock); //WAS MISSPLACED
        /*unlock the group */
        pthread_rwlock_unlock(&((group_r *) group)->lock);
    } else {
        /* if the group isn't found we unlock the groups table */
        pthread_rwlock_unlock(&groups->lock);
    }
    if (l != NULL) {
        free_list(l);
    }
    error = send_response_ls(error, NULL, client_sock);
    write_error_handler(error, *group_id, req, client_sock, timestamp);
    
    return error;
}

/**
 * @brief Gets the value associated to the provided key,to a provided group, as requested by a client

 *  checks if the parameters are correct, if not returns error
    processes the request given by client
    searches the groups table for the provided group
    if the group exists sends a response to the client containing the value associated to the given key

 * @param group_id identifier of the group
 * @param req request provided by the client
 * @param client_sock client socket
 * @param timestamp identifier of the client
 * @return 1 on success or error
 */
int server_get_value(char ** group_id, request_ls *req, int client_sock, clock_t * timestamp) {
    void * request_res;
    int error;
    if (req->n_elements_array != 1) {
        error = send_error_response_ls(WRONG_NUMBER_OF_ARGUMENTS, client_sock);
        write_error_handler(error, *group_id, req, client_sock, timestamp);
        return WRONG_NUMBER_OF_ARGUMENTS;
    }
    error = process_request_ls((*req), client_sock, &request_res);
    if (error < 0) {
        error = send_error_response_ls(error, client_sock);
        write_error_handler(error, *group_id, req, client_sock, timestamp);
        return error;
    }
    
    char * key = (char *) request_res;
    void * value;
    void * group;
    /* read lock the groups table to check if the group still exists */
    pthread_rwlock_rdlock(&groups->lock);
    error = ht_search(groups->table, *group_id, &group);
    /* if the groups exists */
    if (error >= 0) {
        /* read lock the group */
        pthread_rwlock_rdlock(&((group_r *) group)->lock);
        /* unlock groups table*/
        pthread_rwlock_unlock(&groups->lock);
        /* find the value and send it to the client */
        error = ht_search(((group_r *) group)->g_table, key, &value);
        if (error >= 0) {
            error = send_response_ls(error, (kvs *) value, client_sock);
        }
        if (error < 0) {
            error = send_error_response_ls(error, client_sock);
        }
        /* unlock the group*/
        pthread_rwlock_unlock(&((group_r *) group)->lock);
        write_error_handler(error, *group_id, req, client_sock, timestamp);
    } else {
        /* if the group does't exist unlock groups table*/
        pthread_rwlock_unlock(&groups->lock);
    }
    
    return error;
}

/**
 * @brief Deletes the value associated to the provided key,to a provided group, as requested by a client

 *  checks if the parameters are correct, if not returns error
    processes the request given by client
    searches the groups table for the provided group
    if the group given exists, takes the callback list associated to the key-value
    if the callback list is not empty, for each member of the list calls the function send_callback
    deletes the value associated to the given key

 * @param group_id identifier of the group
 * @param req request given by the client
 * @param client_sock client socket
 * @param timestamp identifier of the client
 * @return 1 on success or error
 */

int server_delete_value(char ** group_id,request_ls *req, int client_sock, clock_t * timestamp) {
    void * group;
    void * request_res;
    int error;
    void * value;
    if (req->n_elements_array != 1) {
        error = send_error_response_ls(WRONG_NUMBER_OF_ARGUMENTS, client_sock);
        write_error_handler(error, *group_id, req, client_sock, timestamp);
        return WRONG_NUMBER_OF_ARGUMENTS;
    }
    
    error = process_request_ls((*req), client_sock, &request_res);
    if (error < 0) {
        write_error_handler(send_error_response_ls(error, client_sock), *group_id, req, client_sock, timestamp);
        return error;
    }
    char * key = (char *) request_res;
    
    /* read lock the groups table to check if the group still exists */
    pthread_rwlock_rdlock(&groups->lock);
    error = ht_search(groups->table, *group_id, &group);
    /* if the groups exists */
    if (error >= 0) {
        /* read lock the group */
        pthread_rwlock_wrlock(&((group_r *) group)->lock);
        /* unlock groups table*/
        pthread_rwlock_unlock(&groups->lock);
        /* find and delete the value */
        error = delete_key_return_value(((group_r *) group)->g_table, key, &value);
        pthread_mutex_lock(&accept_callback_lock);
        if (error >= 0) { // WAS MISSING THE ERROR CHECK
            print_list(((kvs *) value)->callback);
            free_list(((kvs *) value)->callback);
            free_kvs_item((kvs *) value);
        }
        pthread_mutex_unlock(&accept_callback_lock);
        /* unlock the group*/
        pthread_rwlock_unlock(&((group_r *) group)->lock);
        
    
    } else {
        /* if the group does't exist unlock groups table*/
        pthread_rwlock_unlock(&groups->lock);
    }
    error = send_response_ls(error, NULL, client_sock);
    write_error_handler(error, *group_id, req, client_sock, timestamp);
    return error;
}

/**
 * @brief Accepts a connection request by a client to the local server ( if the group and password given are correct )

 *  checks if the parameters are correct, if not returns error
    processes the request given by client
    checks if the group-secret pair is correct with check_secret
    checks the groups hashtable to see if the group exists
    if the group-secret is correct adds the client ( with its current info ) to the connected_clients list

 * @param group group that the client is trying to connect
 * @param req request given by the client
 * @param client_sock client socket
 * @param timestamp identifier of the client
 * @return null or error
 */
int accept_request(char ** group, request_ls *req, int client_sock, clock_t * timestamp) {
    int error = 0;
    void * aux;
    
    if (req->n_elements_array != 2) {
        send_error_response_ls(WRONG_NUMBER_OF_ARGUMENTS, client_sock);
        return WRONG_NUMBER_OF_ARGUMENTS;
    }
    
    void * request_res;
    error = process_request_ls((*req), client_sock, &request_res);
    if (error < 0) {
        (void) send_error_response_ls(error, client_sock);
        return error;
    }
    
    char * group_id = ((char **) request_res)[0];
    char * secret = ((char **) request_res)[1];
    
    if ((error = check_secret(group_id, secret)) < 0) {
        free(group_id);
        free(secret);
        free(request_res);
        send_error_response_ls(error, client_sock);

        return error;
    }
    
    /*read lock groups table*/
    pthread_rwlock_rdlock(&groups->lock);
    /*search for the group*/
    error = ht_search(groups->table, group_id, &aux);
    pthread_rwlock_unlock(&groups->lock);
    /*unlock groups table*/
    if (error < 0) {
        free(group_id);
        free(secret);
        free(request_res);
        send_error_response_ls(GROUP_NOT_FOUND, client_sock);
        return GROUP_NOT_FOUND;
    }
    
    /* sends response to client successful connection */
    error = send_response_ls(error, NULL, client_sock);
    
    if (error >= 0) {
        (*group) = group_id;
        client_info client;
        time_t rawtime;
        time(&rawtime);
        client.timestamp = clock();
        client.start = localtime(&rawtime);
        *timestamp = client.timestamp;
        client.end = NULL;
        client.pid = req->client_pid;
        pthread_mutex_lock(&list_lock);
        add_to_list(connected_clients, &client);
        pthread_mutex_unlock(&list_lock);
    }
    
    free(secret);
    free(request_res);

    return error;
}

int check_request(request_ls * req, char ** group_id, int client_sock, clock_t * timestamp, int * client_callback_sock) {
    switch(req->request_type){
        case CONNECTION_REQUEST:
            return accept_request(group_id, req, client_sock, timestamp);
        case PUT_VALUE:
            return server_put_value(group_id, req, client_sock, timestamp);
        case GET_VALUE:
            return server_get_value(group_id, req, client_sock, timestamp);
        case DELETE_VALUE:
            return server_delete_value(group_id, req, client_sock, timestamp);
        case REGISTER_CALLBACK:
            return server_register_callback(group_id, req, client_sock, timestamp, client_callback_sock);
        case CLOSE_REQUEST:
            return close_client(group_id, req, client_sock, timestamp);
        default:
            return INVALID_REQUEST;
    }
}

void * process_client_thread(void * client_socket){
    int  client_sock = *(int *)client_socket;
    int call_back_sock = -1;
    int check_res = 0;
    request_ls req;
    char * group_id = NULL;
    ssize_t read_res = 0;
    clock_t timestamp;
    
    
    while (((read_res = read(client_sock, &req, sizeof(request_ls))) > 0)) {
        if (read_res > 0) {
            if((check_res = check_request(&req, &group_id, client_sock, &timestamp, &call_back_sock)) >= 0){
                if (req.request_type == CLOSE_REQUEST) {
                    return NULL;
                }
            } else {
                if (req.request_type == CONNECTION_REQUEST) {
                    return NULL;
                }
            }
        }
    }
    return NULL;
}

int number_of_key_value_pairs(char * group_id){
    void * group;
    int error;
    if (groups == NULL) {
        return GROUPS_TABLE_DOESNT_EXIST;
    }
    if (group_id == NULL) {
        return NO_GROUP;
    }
    
    pthread_rwlock_rdlock(&groups->lock);
    error = ht_search(groups->table, group_id, &group);
    if (error < 0) {
        printf("Group %s - Not found\n", group_id);
        pthread_rwlock_unlock(&groups->lock); //WAS MISSING DEADLOCK WHEN GROUP DIDN'T EXIST
        return GROUP_NOT_FOUND;
    }
    pthread_rwlock_rdlock(&((group_r *) group)->lock);
    pthread_rwlock_unlock(&groups->lock);
    int res = (int) number_of_items(((group_r *) group)->g_table);
    pthread_rwlock_unlock(&((group_r *) group)->lock);
    
    return res;
}

void print_user_options(void) {
    printf("--------------//---------------\n");
    printf("Commands:\n");
    printf("1 - Create Group (up to 50 characters)\n");
    printf("2 - Delete group\n");
    printf("3 - Show group info\n");
    printf("4 - Show application status\n");
    printf("999 - Close Server\n");
    printf("--------------//---------------\n");
    return;
}

void read_group_id(char group_id[MAX_CHARACTERS]) {
    printf("Insert a group name: ");
    scanf("%s", group_id);
    printf("\n");
}

int check_command(char * command){
    if(strcmp(command,"1") == 0)
        return 1;
     if(strcmp(command,"2") == 0)
        return 2;
    if(strcmp(command,"3") == 0)
        return 3;
    if(strcmp(command,"4") == 0)
        return 4;
    if(strcmp(command,"999")  == 0)
        return 999;
        
    return 0;
}

void UI(pthread_t t_id) {
    char command[5];
    int error = 0;
    char group_id[MAX_CHARACTERS];
    char * secret = NULL;
    char * group_name = NULL;
    int kvpair = 0;
    // REMOVED UNUSED VARIABLE AND COMMENTED CODE
    while (1) {
        print_user_options();
        printf("\nOption: ");
        scanf("%s", command);
        printf("\n");
        switch(check_command(command)){
            case 1:
                read_group_id(group_id);
                if ((error = alloc_string(&group_name, group_id)) < 0) {
                    catch_error(error);
                    break;
                }
                catch_error(create_group(group_name)); //WAS PASSING GROUP ID INSTEAD OF GROUPNAME LEAD TO PROBLEMS WITH FREEING OBJECT NOT ALLOCATED
                break;
            case 2:
                read_group_id(group_id);
                if ((error = alloc_string(&group_name, group_id)) < 0) {
                    catch_error(error);
                    break;
                }
                error = delete_group(group_name);
                free(group_name);
                if(error < 0){
                    catch_error(error);
                }
                break;
            case 3:
                read_group_id(group_id);
                if ((error = alloc_string(&group_name, group_id)) < 0) {
                    catch_error(error);
                    break;
                }
                get_group_secret(group_name, &secret);
                kvpair = number_of_key_value_pairs(group_name);
                if(secret == NULL){
                    free(group_name);
                    printf("Invalid group\n");
                    break;
                }
                if (kvpair < 0) {
                    break;
                }
                printf("Group Info:\n");
                printf("Secret: %s\n", secret);
                printf("Number of key-value pairs: %d\n",kvpair);
                free(group_name);
                free(secret);
                break;
            case 4:
                pthread_mutex_lock(&list_lock);
                printf("Currently connected clients:\n");
                print_list(connected_clients);
                printf("Previously connected clients:\n"); //CHANGE THE : WITH THE \n
                print_list(previously_connected_clients);
                pthread_mutex_unlock(&list_lock);
                break;
            default:
                printf("Invalid command\n");
        }
    }
}

void  * create_server(void * args) {
    struct sockaddr_un server_addr;
    memset(&server_addr, 0, sizeof(struct sockaddr_un));
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, SOCKNAME);

    unlink(SOCKNAME);
    
    server_sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (server_sock < 0) {
        perror("socket creation\n");
        exit(-1);
    }

    if (bind(server_sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr_un)) == -1) {
        perror("bind\n");
        exit(-1);
    }
    if (listen(server_sock, 5) < 0) {
        perror("listen\n");
        exit(-1);
    }
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 1;
    if (setsockopt(server_sock, SOL_SOCKET, SO_RCVTIMEO, (const char *)&tv, sizeof(tv)) < 0) {
        perror("setsockopt");
        exit(-1);
    }
    int client_sock;
    while (1) {
        client_sock = accept(server_sock, NULL, NULL);
        pthread_t t_id;
        pthread_create(&t_id, NULL, process_client_thread, (void *) &client_sock);
    }
    return NULL;
}

void  create_socket_callbacks(void) {
    struct sockaddr_un server_addr;
    memset(&server_addr, 0, sizeof(struct sockaddr_un));
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, CALLBACK_SOCK);

    unlink(CALLBACK_SOCK);
    
    callback_sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (callback_sock < 0) {
        perror("socket creation\n");
        exit(-1);
    }
    
    if (bind(callback_sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr_un)) == -1) {
        perror("bind\n");
        exit(-1);
    }
    if (listen(callback_sock, 5) < 0) {
        perror("listen\n");
        exit(-1);
    }
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 1;
    if (setsockopt(callback_sock, SOL_SOCKET, SO_SNDTIMEO, (const char *)&tv, sizeof(tv)) < 0) {
        perror("setsockopt");
        exit(-1);
    }
    
    if (pthread_mutex_init(&accept_callback_lock, NULL) != 0) {
        perror("mutex intit failed\n");
        exit(-1);
    }
}


void run_server(void) {
    pthread_t t_id;
    int error;
    signal(SIGPIPE, SIG_IGN);
    create_socket_to_auth(); //REMOVED A COMMENT
    create_socket_callbacks();
    connected_clients = create_list(create_client, free_client, get_client_id, print_client, modify_client);
    previously_connected_clients = create_list(create_client, free_client, get_client_id, print_client, modify_client);
    if((error = create_groups_ht()) < 0){
        printf("Couldn't create groups hashtable\n");
        catch_error(error);
    }
    if (pthread_mutex_init(&list_lock, NULL) != 0) {
        perror("list lock mutex init has failed\n");
        exit(-1);
    }
    if (pthread_mutex_init(&auth_lock, NULL) != 0) {
        perror("mutex intit failed\n");
        exit(-1);
    }
    pthread_create(&t_id, NULL, create_server, NULL);
    UI(t_id);
}
