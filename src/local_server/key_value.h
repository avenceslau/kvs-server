//
//  key_value.h
//  server.c
//
//  Created by André Venceslau on 28/05/2021.
//

#ifndef key_value_h
#define key_value_h

#include <stdio.h>
#include "HASH-Table.h"
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

typedef struct value_t value_t;

hash_item* create_k_v_item(char* key, void* value);
hash_item * free_k_v_item(hash_item* item);

#endif /* key_value_h */
