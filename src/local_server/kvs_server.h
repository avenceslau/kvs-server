#ifndef kvs_server_h
#define kvs_server_h
#include "HASH-Table.h"

void run_server(void);
int get_group_secret(char * group_id, char **secret);

#endif /* kvs_server_h */

