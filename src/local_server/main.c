//
//  main.c
//  server.c
//
//  Created by André Venceslau on 13/05/2021.
//

#include <stdio.h>
#include "kvs_server.h"

int main(int argc, const char * argv[]) {
    run_server();
    return 0;
}
