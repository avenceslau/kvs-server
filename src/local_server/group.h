//
//  groups.h
//  server.c
//
//  Created by André Venceslau on 17/05/2021.
//

#ifndef groups_h
#define groups_h

#include <stdio.h>
#include <stdbool.h>
#include <pthread.h>
#include "HASH-Table.h"
#define GROUP_NAME_SIZE 50

typedef struct {
    char * group_id;
    pthread_rwlock_t lock;
    hash_table * g_table;
} group_r;

typedef struct {
    hash_table * table;
    pthread_rwlock_t lock;
} groups_ht;

void free_group(group_r * item);
int create_group(char * group_id);
hash_item* create_group_item(char* key, void* value);
hash_item * free_group_item(hash_item* item);

void free_kvs_item(void* item);
int delete_from_auth(char * group_id); //to be declared else where
int delete_group(char * group_id);

int create_groups_ht(void);
#endif /* groups_h */
