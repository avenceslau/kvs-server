//
//  groups.c
//  server.c
//
//  Created by André Venceslau on 17/05/2021.
//

#include "group.h"
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include "HASH-Table.h"
#include "errors.h"
#include "request.h"
#include "response.h"
#include <netinet/in.h>

extern groups_ht * groups;
extern int auth_sock;
extern struct sockaddr_in auth_addr;
extern pthread_mutex_t auth_lock;


hash_item* create_item(char* key, void* value) {
    // Creates a pointer to a new hash table item
    size_t malloc_size;
    hash_item* item = (hash_item*)malloc(sizeof(hash_item));
    if (item == NULL)
        return NULL;
    malloc_size = strlen(key) + 1;
//    if (malloc_size > 51) {
//        printf("[1] was going to alloc %lu\n", malloc_size);
//        free(item);
//        return NULL;
//    } 
    item->key = (char*)malloc(malloc_size);
    if (item->key == NULL){
        free(item);
        return NULL;
    }
    malloc_size = strlen(value) + 1;
    item->value = (char*) malloc(malloc_size);
    if (item->value == NULL){
        free(item->key);
        free(item);
        return NULL;
    }
    strcpy(item->key, key);
    strcpy(item->value, value);
    item->next = NULL;

    return item;
}

/**
 * @brief frees a kvs item
 * 
 * @param item item to be freed
 */
void free_kvs_item(void* item) {
    if (item == NULL) {
        return;
    }
    kvs * aux = (kvs *) item;
    if (aux->value != NULL) {
        free(aux->value);
        aux->value = NULL;
        free(aux);
        return;
    }
    free(aux);
}

/**
 * @brief frees a info related to a group
 *        
 * @param item  item
 */
void free_group_info(group_r * item) {
    free_table(item->g_table, free_kvs_item);
    free(item->group_id);
}

/**
 * @brief destroys a group
 * 
 * @param group  group to be destroyed
 */
void destroy_group(void * group){
    if(group == NULL)
        return;
    group_r * aux = (group_r *) group;
    pthread_rwlock_wrlock(&aux->lock);
    pthread_rwlock_unlock(&aux->lock);
    free_table(aux->g_table, free_kvs_item);
    pthread_rwlock_destroy(&aux->lock);
    free(aux);
}

/**
 * @brief creates a string with 24 randomly generated characters
 *        contains letters and numbers only
 * 
 * @return char* returns the string
 */
char* gen_secret(void){
    int aux;
    char* pw = (char*) malloc(SECRET_SIZE+1);
    for(int i = 0; i < SECRET_SIZE;i++){
        switch(rand()%3){
            case(0):
                aux = rand()%9 + 48;
                break;
            case(1):
                aux = rand()%25 + 65;
                break;
            default:
                aux = rand()%25 + 97;
                break;
        }
        pw[i] = (char) aux;
    }
    pw[SECRET_SIZE] = '\0'; //PRINT MISSPLACED
    return pw;
}

/**
 * @brief registers a group id on the authentification server
 * 
 * @param group_id group_id
 * @return int 0 on a success, on a fail returns one of the following errors
 *         FAILED MALLOC, AUTH OFFLINE or GROUP ALREADY EXISTS
 */
int register_group_auth(char * group_id){
    char request[BUFFER_SIZE];
    char response[BUFFER_SIZE];
    int error;
    response_as res;
    char * secret = gen_secret(); //REMOVED THE DEBUG CASE
    if (secret == NULL) {
        return FAILED_MALLOC;
    }
    
    if((error = create_resquest_as(NEW_GROUP_AS, request, group_id, secret)) < 0) {
        free(secret);
        return error;
    }
    
    pthread_mutex_lock(&auth_lock);
    error = send_request_as(auth_sock, request, auth_addr, sizeof(auth_addr));
    if(read_response_as(auth_sock, response) > 0) {
        pthread_mutex_unlock(&auth_lock);
        res = unfuse_response_as(response);
        if (res.response_result == NULL) {
            return AUTH_OFFLINE;
        }
        if (strcmp(res.response_result, REQUEST_UNFULFILLED) == 0) {
            return GROUP_ALREADY_EXISTS;
        }
        printf("\t Group: %s Secret: %s\n", group_id, secret); //PRINT THAT WAS IN THE FUNCTION ABOVE
        return 0;
    }
    pthread_mutex_unlock(&auth_lock);
    return AUTH_OFFLINE;
}

/**
 * @brief Create a group object, and registers it under the auth server
 * 
 * @param group_id group id to be registered
 * @return int returns 0 on a success on fail returns GROUPS TABLE DOESNT EXIST
 *             NO GROUP, FAILED MALLOC, PHTREAD RDLOCK CREATION FAILED, HT CREATION FAILED,
 *             GROUP ALREADY EXISTS, FAILED INSERT
 */
int create_group(char * group_id){
    void * aux;
    int error = 0;
    /*check if the hash table that contains the groups exists*/
    if (groups == NULL) {
        return GROUPS_TABLE_DOESNT_EXIST;
    }

    /*check if the group id has been created*/
    if (group_id == NULL) {
        return NO_GROUP;
    }
    
    if ((error = register_group_auth(group_id)) < 0) {
        return error;
    }
    /*create group*/
    group_r * group = (group_r *) malloc(sizeof(group_r));

    if (group == NULL) {
        free(group_id);
        return FAILED_MALLOC;
    }
    
    group->group_id = group_id;

    if (pthread_rwlock_init(&group->lock, NULL) != 0) {
        free(group->group_id);
        free(group);
        return PTHREAD_RD_LOCK_INIT_FAILED;
    }

    if(create_table(&group->g_table) < 0){
        free(group->group_id);
        pthread_rwlock_destroy(&group->lock);
        free(group);
        return HT_CREATION_FAILED;
    }
    /* end of group creation */
    /*lock hash table that contains the groups */
    pthread_rwlock_wrlock(&groups->lock);
    /*check if the group already exists*/
    error = ht_search(groups->table, group_id, &aux);
    if (error == 0) {
        printf("Group already exists\n");
        pthread_rwlock_unlock(&groups->lock);
        destroy_group(group);
        free(group_id);
        return GROUP_ALREADY_EXISTS;
    }
    /*insert the group*/
    //FIXME resize maybe needed
    //FIXME check size before insertion, insert only if sure of success
    error = ht_insert(&groups->table, group_id, (void *) group, destroy_group);
    pthread_rwlock_unlock(&groups->lock);
    /*unlock groups hash table*/
    /*destroy group on failed insert*/
    if (error < 0) {
        delete_from_auth(group_id);
        destroy_group(group);
        free(group_id);
        return FAILED_INSERT;
    }
    
    return 0;
}


/**
 * @brief removes a group from the auth server
 * 
 * @param group_id group that will be removed
 * @return int 0 on a succsess or AUTH OFFLINE
 */
int delete_from_auth(char * group_id){
    char request[BUFFER_SIZE];
    char response[BUFFER_SIZE];
    int error;
    response_as res;
    
    create_resquest_as(DELETE_GROUP_AS, request, group_id, NULL);
    for (int i = 0; i < 10 ; i++) {
        error = send_request_as(auth_sock, request, auth_addr, sizeof(auth_addr));
        nsleep(30000);
        if(read_response_as(auth_sock, response) > 0) {
            res = unfuse_response_as(response);
            if (res.response_result == NULL) {
                return AUTH_OFFLINE;
            }
            return 0;
        }
    }
    return AUTH_OFFLINE;
}

/**
 * @brief deletes a group from the local server, includes the removal from
 *        the auth server
 * 
 * @param group_id group to be deleted
 * @return int 0 on a success, on a fail returns errors
 */
int delete_group(char * group_id){
    int error;
    void * group;
    if (groups == NULL) {
        return GROUPS_TABLE_DOESNT_EXIST;
    }
    if (group_id == NULL) {
        return NO_GROUP;
    }
    pthread_rwlock_rdlock(&groups->lock);
    error = ht_search(groups->table, group_id, &group);
    pthread_rwlock_unlock(&groups->lock);
    if (error < 0) {
        return GROUP_NOT_FOUND;
    }
    
    for (int numsec = 1; numsec <= MAXSLEEP; numsec<<=1) {
        pthread_mutex_lock(&auth_lock);
        if((error = delete_from_auth(group_id)) >= 0){
            pthread_mutex_unlock(&auth_lock);
            pthread_rwlock_wrlock(&groups->lock);
            error = delete_key_return_value(groups->table, group_id, &group);
            pthread_rwlock_unlock(&groups->lock);
            if (error < 0) {
                return error;
            }
            
            destroy_group((group_r *) group);
            
            return 0;
        }
        pthread_mutex_unlock(&auth_lock);
        if (error != SERVER_NOT_FOUND) {
            return error;
        }
    }
    return CONNECTION_FAILED;
}

/**
 * @brief Create a groups ht object
 * 
 * @return int 0 on a success
 */
int create_groups_ht(void){
    srand((unsigned) time(NULL));
    if (groups != NULL) {
        return GROUPS_TABLE_ALREADY_EXISTS;
    }
    groups = (groups_ht *) malloc(sizeof(groups_ht));
    if(pthread_rwlock_init(&groups->lock, NULL) < 0){
        free(groups);
        return PTHREAD_RD_LOCK_INIT_FAILED;
    }
    
    //FIXME not free_item use special case function for group
    if (create_table(&groups->table) < 0) {
        pthread_rwlock_destroy(&groups->lock);
        free(groups);
        return HT_CREATION_FAILED;
    }

    return 0;
}
